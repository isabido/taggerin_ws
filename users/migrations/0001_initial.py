# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
        ('Company', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('user', models.OneToOneField(primary_key=True, to=settings.AUTH_USER_MODEL, serialize=False)),
                ('user_type', models.IntegerField(choices=[(0, 'Company'), (1, 'Normal')], null=True)),
                ('profile_image', models.URLField(blank=True, max_length=255, null=True)),
                ('gender', models.CharField(null=True, max_length=1)),
                ('birthday', models.DateField(null=True)),
                ('rfc', models.CharField(null=True, max_length=13)),
                ('category', models.ForeignKey(to='Company.Category', related_name='company_category')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
