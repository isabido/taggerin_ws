__author__ = 'isabido'
from users.views import UserViewSet
from rest_framework.routers import DefaultRouter
from rest_framework import renderers

NormalUser_create = UserViewSet.as_view({'get': 'retrieve', 'post': 'create', 'put': 'update'})