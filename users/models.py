from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from Company.models import TagOffered, Category
# Notice that we are importing the gis models here


# Create your models here.
class BaseProfile(models.Model):
    USER_TYPES = ((0, 'Company'),(1, 'Normal'),)
    user = models.OneToOneField(User, primary_key=True, related_name="user_profile")
    user_type = models.IntegerField(null=True, choices=USER_TYPES)
    profile_image = models.URLField(max_length=255, blank=True,null=True)
    class Meta:
        abstract = True

class NormalProfile(models.Model):
    gender = models.CharField(max_length=1, null=True)
    birthday = models.DateField(auto_now_add=False, null=True)
    class Meta:
        abstract = True

class CompanyProfile(models.Model):

    category = models.ForeignKey(Category, null=True, related_name="company_category")
    rfc = models.CharField(max_length=13, null=True)

    class Meta:
        abstract = True

class Profile(CompanyProfile, NormalProfile, BaseProfile):
    pass

class FriendList(models.Model):
    user = models.ForeignKey(User, related_name="user_list")
    friend = models.IntegerField()