from django.http import Http404
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.views import APIView
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import permissions

from users.UserSerializers.NormalSerializer import NormalUser
from oauth2_provider.ext.rest_framework import OAuth2Authentication
from users.TaggerinPermissions.TGPermissions import TgPermissions
from users.permissions import UserInSessionPermission, UserTypeCompany, isFreePlan, UserTypeNormal


# Create your views here.
#class NormalSignUp(APIView):
class UserViewSet(viewsets.ViewSet):
    """
    A viewset for viewing and editing user instances.
    """
    def get_object(self, pk):
        try:
            obj = User.objects.get(pk=pk)
            #self.check_object_permissions(self.request, obj)
            return obj
        except User.DoesNotExist:
            raise Http404
    authentication_classes = (OAuth2Authentication,)
    #permission_classes = (permissions.IsAuthenticated, UserInSessionPermission, UserTypeCompany,)

    def get_permissions(self):
        print("metodo")
        if self.request.method == 'POST':
            return (permissions.AllowAny(),)

        return (UserInSessionPermission(), UserTypeNormal(), )

    # def create(self, request):
    #    # permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    #     # Both UserProfileSerializer and UserProfileSerializer are required
    #     # in order to validate and save data on their associated models.
    #     try:
    #
    #         user_serializer = NormalUser(data=request.data)
    #         #user_profile_serializer = NormalProfileUser(data=request.data)
    #
    #         if user_serializer.is_valid():
    #             #user = user_serializer.save()
    #             user_serializer.save()
    #
    #             return Response(user_serializer.data, status=status.HTTP_201_CREATED)
    #         # Combine errors from both serializers.
    #         errors = dict()
    #         return Response(errors, status=status.HTTP_400_BAD_REQUEST)
    #     except Exception as e:
    #         return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)

    def retrieve(self, request, pk=None):
        try:
            if request.user and request.auth:
                user = self.get_object(request.user.pk)
            else:
                return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)

            # user = User.objects.filter(pk=request.user.pk).select_related()
            #user2 = User.objects.filter(id=user.id).exclude(user_tagoffered=True)

            #pprint(user2.user_tagoffered)
            #serializer = GetCompanySerializer(user)
            return Response({"good":"for you"}.data, status=status.HTTP_200_OK)
        except ValidationError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)

    def create(self, request):
        # permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
        # Both UserProfileSerializer and UserProfileSerializer are required
        # in order to validate and save data on their associated models.
        try:
            if request.auth:
                error = {"error":"You don't have permission"}
                return Response(error,status=status.HTTP_401_UNAUTHORIZED)

            user_serializer = NormalUser(data=request.data)
            #user_profile_serializer = NormalProfileUser(data=request.data)

            if user_serializer.is_valid():
                #user = user_serializer.save()
                user_serializer.save()

                return Response(user_serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(user_serializer.errors, status=status.HTTP_428_PRECONDITION_REQUIRED)
        except ValidationError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)

    def update(self, request, pk=None):
        if request.user and request.auth:
            user = self.get_object(request.user.pk)
        else:
            return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)
        user_serializer = NormalUser(user, data=request.data)
        if user_serializer.is_valid():
            user_serializer.save()
            #user_serializer.save(#owner=self.request.user)
            return Response(user_serializer.data, status=status.HTTP_200_OK)
        errors = dict()
        errors.update(user_serializer.errors)
        return Response(errors, status=status.HTTP_400_BAD_REQUEST)