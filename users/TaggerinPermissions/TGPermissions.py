import datetime
from pprint import pprint
from payments.PaymentAdapter import PaymentAdapter
from payments.models import Subscription
import arrow
from plans.models import Plan

__author__ = 'isabido'
class TgPermissions(object):
    def __init__(self):
        self.user = None
        self.subscription = None
        self.plan = None
        self.conversation = None
        self.message = None
        self.payments = None

    def isFreePlan(self, user):
        self.subscription = Subscription.objects.get(user= user.pk)

        #pprint(vars(subscription))
        print("PLAN: " + self.subscription.plan.name)
        if(self.subscription.plan.name == "PLAN LIGHT"):
            return True
        return False

    def isSubscriptionValid(self, user):
        self.user = user
        self.plan = Plan.objects.get(name='PLAN LIGHT')
        self.subscription = Subscription.objects.get(user_id=user.id)
        if self.subscription.plan.name != "PLAN LIGHT":
            creation_date = arrow.get(self.subscription.creation_date)
            end_date = arrow.get(self.subscription.period_end_date)
            current_date = arrow.now()
            if(current_date <= end_date):
                return True
            else:
                band = self.isValidateOPSubscription()
                return band
        elif self.subscription.plan.name == "PLAN LIGHT" :
            return True

        return False

    def changeSubscription(self, user, plan):
        try:
            print("change")
            self.subscription.op_subscription = "Taggerin2"
            self.subscription.creation_date = arrow.now().isoformat()
            self.subscription.status = "Active"
            self.subscription.period_end_date = datetime.date.today()+datetime.timedelta(days=5000)
            self.subscription.charge_date = datetime.date.today()
            self.subscription.trial_end_date = datetime.date.today()
            self.subscription.op_plan_id = "Taggerin"
            self.subscription.current_period_number = 0
            self.subscription.plan_id=plan
            self.subscription.user_id = user.id
            self.subscription.save()
            return True
        except Exception as e:
            return False


    def changeSubscription(self, user, plan, op_subscription):
        try:
            print("change")
            self.subscription.op_subscription = "Taggerin2"
            self.subscription.creation_date = arrow.now().isoformat()
            self.subscription.status = "Active"
            self.subscription.period_end_date = datetime.date.today()+datetime.timedelta(days=5000)
            self.subscription.charge_date = datetime.date.today()
            self.subscription.trial_end_date = datetime.date.today()
            self.subscription.op_plan_id = "Taggerin"
            self.subscription.current_period_number = 0
            self.subscription.plan_id=self.plan.id
            self.subscription.user_id = self.user.id
            self.subscription.save()
            return True
        except Exception as e:
            return False

    def isValidateOPSubscription(self):
        try:
            print(self.subscription.user.op_customer_user.customer_id)
            op = PaymentAdapter()
            op.getCustomer(self.subscription.user.op_customer_user.customer_id)
            op_subscripcion=op.getSubscription(self.subscription.op_subscription)
            if(op_subscripcion['status'] != 'active'):
                self.changeSubscription(self.user, self.plan.id)
                return True
        except Exception as e:
            return False
        return True