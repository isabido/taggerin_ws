from rest_framework_gis.serializers import GeoFeatureModelSerializer

__author__ = 'isabido'
from rest_framework import serializers
from django.contrib.auth.models import User
from users.models import Profile

class NormalUser(GeoFeatureModelSerializer):
    profile_image = serializers.URLField(source='profile.profile_image')
    address = serializers.CharField(source='profile.gender')

    #owner = serializers.ReadOnlyField(source='owner.username')
    class Meta:
        model = User
        fields = ['id','username','email', 'password','profile_image', 'gender', 'birthday',]

    def create(self, validated_data):
        _post = validated_data.pop('profile')
        user = User.objects.create_user(**validated_data)
        Profile.objects.create(user=user, **_post)
        return user

    def update(self, instance, validated_data):
        _post = validated_data.pop('profile')
        instance.username = validated_data.get('username', instance.username)
        instance.email = validated_data.get('email', instance.email)
        instance.password = validated_data.get('password', instance.password)
        instance.save()
        prof = Profile.objects.get(pk=instance.id)
        prof.profile_image = _post['profile_image']
        prof.save()

        #Profile.objects.update(user=user, profile_image = _post['profile_image'])

        return instance
