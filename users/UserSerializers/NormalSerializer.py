__author__ = 'isabido'

from rest_framework import serializers
from django.contrib.auth.models import User
from users.models import Profile
from django.db import transaction, IntegrityError, Error

class NormalUser(serializers.ModelSerializer):
    profile_image = serializers.URLField(required=False)
    gender = serializers.CharField()
    birthday = serializers.DateField()
    #user_type = serializers.IntegerField(source='profile.user_type')
    class Meta:
        model = User
        fields = ['id','first_name','email', 'password','profile_image', 'gender', 'birthday',]

    def create(self, validated_data):
        try:
            print("entra")
            user_email = User.objects.filter(email=validated_data.get('email'))
            print(len(user_email))
            if len(user_email) > 0:
                print("email exist")
                raise Exception("email exist")
            #_post = validated_data.pop('profile')
            p_image = validated_data.pop('profile_image')

            gender = validated_data.pop('gender')
            birthday = validated_data.pop('birthday')
            user = User.objects.create_user(username=validated_data.get('email'), **validated_data)
            user.is_active = True
            user.save()

            prof=Profile.objects.create(user=user, user_type=1, birthday=birthday, profile_image=p_image, gender=gender, category_id = 1 )
            prof.save()

            dict_user = {
                'id': user.id,
                'email': user.email,
                "username": user.username,
                "profile_image": prof.profile_image,
                "gender": prof.gender,
                "birthday":prof.birthday,
                "password": user.password,
                "first_name": user.first_name
                    }
            return dict_user
        except Error as e:
            raise serializers.ValidationError(e)
        except IntegrityError as e:
            raise serializers.ValidationError(e)
        except Profile.DoesNotExist as e:
             raise serializers.ValidationError(e)
        except Exception as e:
            raise serializers.ValidationError(e)

    def update(self, instance, validated_data):
        try:
            _post = validated_data.pop('profile')
            instance.username = validated_data.get('username', instance.username)
            instance.email = validated_data.get('email', instance.email)
            instance.set_password(validated_data.get('password', instance.password))
            #instance.password = validated_data.get('password', instance.password)
            instance.save()
            prof = Profile.objects.get(pk=instance.id)
            prof.profile_image = _post['profile_image']
            prof.birthday = _post['birthday']

            if prof.user_type != 1:
                prof.user_type = 1
            prof.save()
        except Error as e:
            raise serializers.ValidationError(e)
        except IntegrityError as e:
            raise serializers.ValidationError(e)
        except Exception as e:
            raise serializers.ValidationError(e)
        #Profile.objects.update(user=user, profile_image = _post['profile_image'])

        return instance


class NormalProfileUser(serializers.ModelSerializer):
   class Meta:
        model = Profile
        #profile_image = Field(source='profile.profile_image')
        fields = ['profile_image',]

        def create(self, validated_data):
            _post = validated_data.pop('profile_image')
            user = User.objects.create_user(**validated_data)
            return Profile(user=user)
