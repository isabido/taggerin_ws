from pprint import pprint
from payments.models import Subscription

__author__ = 'isabido'
from rest_framework import permissions
from django.http import Http404
from users.models import Profile

class UserInSessionPermission(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """

    def has_object_permission(self, request, view, obj):
        # Write permissions are only allowed to the owner of the snippet.
        return obj == request.user

class UserTypeCompany(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """
    def has_permission(self, request, view):
        # Write permissions are only allowed to the owner of the snippet.
        user = Profile.objects.get(pk= request.user.pk)
        return user.user_type == 0

class BothTypeCompany(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """
    def has_permission(self, request, view):
        # Write permissions are only allowed to the owner of the snippet.
        user = Profile.objects.get(pk= request.user.pk)
        return user.user_type == 0 or user.user_type == 1

class UserTypeNormal(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """
    def has_permission(self, request, view):
        # Write permissions are only allowed to the owner of the snippet.
        user = Profile.objects.get(pk= request.user.pk)
        return user.user_type == 1

    def has_object_permission(self, request, view, obj):
        # Write permissions are only allowed to the owner of the snippet.
        print("object")
        subscription = Subscription.objects.get(pk= obj.pk)
        return False

class isFreePlan(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """
    def has_object_permission(self, request, view, obj):
        # Write permissions are only allowed to the owner of the snippet.
        subscription = Subscription.objects.get(pk= obj.pk)
        print("PLAN: " + subscription.subscription_plan.name)
        if(subscription.subscription_plan.name == "PLAN LIGHT"):
            return True
        return False
    def has_permission(self, request, view):
        # Write permissions are only allowed to the owner of the snippet.
        pprint(request.user)
        return False