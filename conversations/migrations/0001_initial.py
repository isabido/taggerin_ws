# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Conversation',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('unread_messages', models.IntegerField()),
                ('receiver', models.ForeignKey(related_name='user_receiver', to=settings.AUTH_USER_MODEL)),
                ('sender', models.ForeignKey(related_name='user_sender', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('body', models.TextField()),
                ('sent_at', models.DateTimeField()),
                ('status', models.BooleanField(default=False)),
                ('type', models.IntegerField(choices=[(0, 'Image'), (1, 'Video'), (2, 'File'), (3, 'Geolocation'), (4, 'Message')], null=True)),
                ('attachments', models.CharField(max_length=250)),
                ('conversation_id', models.ForeignKey(related_name='message', to='conversations.Conversation')),
                ('receiver', models.ForeignKey(related_name='message_receiver', to=settings.AUTH_USER_MODEL)),
                ('sender', models.ForeignKey(related_name='message_sender', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
