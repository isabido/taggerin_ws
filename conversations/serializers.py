from django.db import transaction, Error
from django.db.models import Q, F
from django.http import Http404
from Company.models import Notification
from conversations.models import Conversation, Message
from pprint import pprint

__author__ = 'isabido'
from rest_framework import serializers
from django.contrib.auth.models import User

class ContactSerializer(serializers.ModelSerializer):
    profile_image = serializers.URLField(source='user_profile.profile_image')
    class Meta:
        model = User
        fields = ('id', 'first_name', 'username', 'profile_image')

class ReplyCountSerializer(serializers.RelatedField):

    def to_representation(self, instance):
        count = Message.objects.filter(Q(status=False)).count()
        return count

class ConversationSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    receiver = ContactSerializer()
    sender = ContactSerializer()
    unread_messages_sender = serializers.SerializerMethodField('getUnread_messagesSender')
    unread_messages_receiver = serializers.SerializerMethodField('getUnread_messagesReceiver')

    def getUnread_messagesSender(self, obj):
        pprint (vars(obj))
        total = Message.objects.filter(receiver=obj.sender, status=False).count()
        return total

    def getUnread_messagesReceiver(self, obj):
        pprint (vars(obj))
        total = Message.objects.filter(receiver=obj.receiver, status=False).count()
        return total


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ('id', 'receiver', 'status', 'sent_at', 'body', 'type', 'attachments', 'conversation_id')

    def create(self, validated_data):
        message = None
        with transaction.atomic():
            try:
                conv = Conversation.objects.get(Q(sender_id = validated_data['sender'].pk) | Q(receiver_id=validated_data['sender'].pk), Q(sender_id = validated_data['receiver']) | Q(receiver_id=validated_data['receiver']))
            except Conversation.DoesNotExist:
                try:


                    #receiver = User.objects.get(pk=int(validated_data['receiver']))

                    conv = Conversation.objects.create(unread_messages=0, receiver=validated_data['receiver'], sender= validated_data['sender'])
                except User.DoesNotExist:

                    raise Http404
                except Error as e:
                    raise Http404
                except Exception as e:
                    raise Http404

            try:
                if conv:

                    validated_data['conversation_id'] = conv

                    message = Message.objects.create(status=False, **validated_data)



                    Conversation.objects.filter(pk=conv.id).update(unread_messages=F('unread_messages')+1)

                else:
                    message = None
            except Exception as e:
                raise Http404
        return message

class ConvesationMessageSerializer(serializers.Serializer):
    unread_messages = serializers.SerializerMethodField('getUnread_messages')
    conversation = ConversationSerializer()

    def getUnread_messages(self, obj):
        pprint (vars(obj._state))
        return 863