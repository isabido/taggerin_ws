from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Conversation(models.Model):
    sender = models.ForeignKey(User, related_name="user_sender")
    receiver = models.ForeignKey(User, related_name='user_receiver')
    unread_messages = models.IntegerField()


class Message(models.Model):
    MESSAGE_TYPES = ((0, 'Image'),(1, 'Video'), (2, 'File'), (3, 'Geolocation'), (4, 'Message'))
    body = models.TextField()
    sent_at = models.DateTimeField(auto_now=False, auto_now_add=False)
    conversation_id = models.ForeignKey(Conversation, related_name = 'message')
    status = models.BooleanField(default=False)
    type = models.IntegerField(null=True, choices=MESSAGE_TYPES)
    attachments = models.CharField(max_length=250, null=True, blank=True)
    sender = models.ForeignKey(User, related_name="message_sender")
    receiver = models.ForeignKey(User, related_name="message_receiver")
    #created_at = models.DateTimeField(auto_now=True)



