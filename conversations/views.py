from pprint import pprint
from django.db import Error
from django.http import Http404
from django.shortcuts import render
from django.contrib.auth.models import User
from django.utils.datastructures import MultiValueDictKeyError
from oauth2_provider.ext.rest_framework import OAuth2Authentication
from rest_framework import viewsets
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from Company.models import Notification
from conversations.models import Conversation, Message
from users import permissions
from users.TaggerinPermissions.TGPermissions import TgPermissions
from users.permissions import UserInSessionPermission, UserTypeCompany
from django.db.models import Q
from rest_framework import status
from rest_framework import permissions
from conversations.serializers import ConversationSerializer, MessageSerializer, ConvesationMessageSerializer
import json

# Create your views here.
class ConversationViewSet(viewsets.ViewSet):
    def get_object(self, pk):
        try:
            obj = User.objects.get(pk=pk)
            #self.check_object_permissions(self.request, obj)
            return obj
        except User.DoesNotExist:
            raise Http404
    authentication_classes = (OAuth2Authentication,)
    renderer_classes = (JSONRenderer, )
    #permission_classes = (permissions.IsAuthenticated, UserInSessionPermission, UserTypeCompany,)

    def get_permissions(self):
        return (permissions.IsAuthenticated(), UserInSessionPermission(), )

    def list(self, request):
        if request.user and request.auth:
            user = self.get_object(request.user.pk)
        else:
            return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)
        try:
            tg_p = TgPermissions()
            tg_p.isSubscriptionValid(request.user)
            try:
                offset = (0, int(request.query_params['offset'])) [request.query_params['offset'] is not None]
                limit = (20, int(request.query_params['limit'])) [request.query_params['limit'] is not None]

                conversation = Conversation.objects.filter(Q(sender = request.user) | Q(receiver=request.user))[offset:limit]
            except MultiValueDictKeyError:
                conversation = Conversation.objects.filter(Q(sender = request.user) | Q(receiver=request.user))


        except Conversation.DoesNotExist:
            conversation = None

        serializer = ConversationSerializer(conversation, many=True)
        #serializer = ConvesationMessageSerializer(listM,many=True)
        # print(serializer.is_valid())
        # if serializer.is_valid(raise_exception=False):
        #     serializer.save(sender=request.user)
        #     return Response({'conversations':serializer.data}, status=status.HTTP_200_OK)
        #serializer.is_valid(raise_exception=False)
        #serializer.data.unread_messages = 101
        #print(serializer.data[0])

        return Response({'conversations':serializer.data}, status=status.HTTP_200_OK)

    def retrieve(self, request, pk=None):
        if request.user and request.auth:
            user = self.get_object(request.user.pk)
        else:
            return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)
        try:
            conversation = Conversation.objects.filter(Q(pk = pk), Q(sender = request.user) | Q(receiver=request.user))
        except Conversation.DoesNotExist:
            return Response({'error': 'This Conversation does not exist. Send a message to create it.'}, status=status.HTTP_400_BAD_REQUEST)
        if conversation:
            try:
                try:
                    offset = (0, int(request.query_params['offset'])) [request.query_params['offset'] is not None]
                    limit = (20, int(request.query_params['limit'])) [request.query_params['limit'] is not None]

                    messages = Message.objects.filter(conversation_id_id = pk).order_by( 'sent_at', 'id')[offset:limit]

                except MultiValueDictKeyError:
                    messages = Message.objects.filter(conversation_id_id = pk).order_by( 'sent_at', 'id')
                messages.update(status=True)
                conversation.update(unread_messages=0)

            except Conversation.DoesNotExist:
                messages = None
            serializer = MessageSerializer(messages, many=True)
            messages_list = {'messages':serializer.data}

            return Response(messages_list, status=status.HTTP_200_OK)
        return Response({'error': 'Conversation does not exist.'}, status=status.HTTP_400_BAD_REQUEST)




class MessageViewSet(viewsets.ViewSet):
    def get_object(self, pk):
        try:
            obj = User.objects.get(pk=pk)
            #self.check_object_permissions(self.request, obj)
            return obj
        except User.DoesNotExist:
            raise Http404
    authentication_classes = (OAuth2Authentication,)
    #permission_classes = (permissions.IsAuthenticated, UserInSessionPermission, UserTypeCompany,)

    def get_permissions(self):
        return (permissions.IsAuthenticated(), UserInSessionPermission(),)

    def create(self, request):
        # permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
        # Both UserProfileSerializer and UserProfileSerializer are required
        # in order to validate and save data on their associated models.
        if not request.auth:
            error = {"error":"You don't have permission"}
            return Response(error,status=status.HTTP_401_UNAUTHORIZED)

        try:
            conv = Conversation.objects.get(Q(sender_id = request.user.pk) | Q(receiver_id=request.user.pk), Q(sender_id = request.data['receiver']) | Q(receiver_id=request.data['receiver']))
        except Conversation.DoesNotExist:
            try:

                receiver = User.objects.get(pk=int(request.data['receiver']))

                conv = Conversation.objects.create(unread_messages=0, receiver=receiver, sender= request.user)
                description = "El usuario {0} le ha enviado un mensaje.".format(request.user.first_name)
                print(description)
                Notification.objects.create(user_id=receiver.id, notification_type=0, description=description, match=int(request.user.id))

            except User.DoesNotExist:

                raise Http404
            except Error as e:
                print("aqui excep error")
        request.data['conversation_id']=conv.pk
        message_serializer = MessageSerializer(data=request.data)
        if message_serializer.is_valid():
            message_serializer.save(sender=request.user)

            print("before resp")
            return Response(message_serializer.data, status=status.HTTP_201_CREATED)
        # Combine errors from both serializers.

        return Response(message_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
