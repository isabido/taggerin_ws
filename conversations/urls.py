from conversations.views import ConversationViewSet, MessageViewSet

__author__ = 'isabido'
ConversationsUser = ConversationViewSet.as_view({'get':'retrieve'})
List_Conversations = ConversationViewSet.as_view({'get': 'list'})
Message = MessageViewSet.as_view({'post':'create'})