from django.shortcuts import render
import openpay
# Create your views here.
from rest_framework import viewsets


class PayViewSet(viewsets.ModelViewSet):
    def create(self, request):
        _page = ProfilePageSerializer(data=request.data)
        if len(request.data['profile_images']) > 3:
            errors = dict()
            errors["error"] = "You have permission to load 3 images"
            return Response(errors, status=status.HTTP_400_BAD_REQUEST)
        #requested_tags = RequestedTagSerializer(data=request.data)
        if _page.is_valid():
            _page.save(user=self.get_object(request.user.pk))
            return Response(_page.data, status=status.HTTP_201_CREATED)
        # Combine errors from both serializers.

        return Response(_page.errors, status=status.HTTP_400_BAD_REQUEST)
