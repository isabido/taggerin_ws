__author__ = 'isabido'
import openpay

class OpenPayService():
    def __init__(self):
        openpay.api_key = "sk_6d71035fd0c2412eb94652869eddc261"
        openpay.verify_ssl_certs = False
        openpay.merchant_id = "mvzmtr64bdwuty29siwj"
        openpay.production = False
        self.payService = openpay

    def createCustomer(self, client):
        customer = self.payService.Customer.create(
                    name=client.name,
                    email=client.email,
                    address={
                            "city": client.city,
                            "state":client.state,
                            "line1":client.address,
                            "postal_code":client.postal_code,
                            "line2":"",
                            "line3":"",
                            "country_code":"MX"
                            },
                    phone_number=client.phone,
                    last_name = client.last_name
                    )
        return customer

    def createCard(self, customer,card):
        card = customer.cards.create(
                card_number=card.number,
                holder_name=card.holder_name,
                expiration_year=card.expiration_year,
                expiration_month=card.expiration_month,
                cvv2=card.cvv,
                address={
                    "city":card.city,
                    "country_code":"MX",
                    "postal_code":card.postal_code,
                    "line1":card.address,
                    "line2":"Roble 207",
                    "line3":"col carrillo",
                    "state":card.state
               })
        return card

    def getCard(self, customer, id_card):
        card = customer.cards.retrieve(id_card)
        return card

    def deleteCard(self, card):
        return card.delete()





