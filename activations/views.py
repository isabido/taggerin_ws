from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from users.permissions import UserInSessionPermission, UserTypeCompany
from rest_framework.permissions import AllowAny
from rest_framework import permissions
from django.http import Http404
from django.contrib.auth.models import User
from rest_framework import status
from oauth2_provider.ext.rest_framework import OAuth2Authentication
from . import utils

# Create your views here.
class ActivationCompanyViewSet(viewsets.ViewSet):

    authentication_classes = (OAuth2Authentication,)
    #permission_classes = (permissions.IsAuthenticated, UserInSessionPermission, UserTypeCompany,)

    def get_permissions(self):
        if self.request.method == 'POST':
            return (permissions.AllowAny(),)

        return (permissions.IsAuthenticated(), UserInSessionPermission(), UserTypeCompany(),)


    def create(self, request, activation_key=None):
        # permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
        # Both UserProfileSerializer and UserProfileSerializer are required
        # in order to validate and save data on their associated models.
        try:
            success = utils.activate_user(activation_key)
            # if not activated
            if success is not False:
                message = {}
                message["message"] = "Activated"
                return Response(message, status=status.HTTP_202_ACCEPTED)
            error = {"error": "Not Activated"}
            return Response(error, status=status.HTTP_400_BAD_REQUEST)
        except ValidationError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)

