import datetime
from pprint import pprint
from django.contrib.auth.models import User
from django.core.exceptions import ImproperlyConfigured, ValidationError
from django.db import transaction
from django.db.models import Q
from django.template.loader import render_to_string
from django.core.mail import send_mail, BadHeaderError
from Company.models import TagOffered
from payments.models import Subscription
from plans.models import Plan
import arrow
from users.models import Profile

__author__ = 'isabido'
import hashlib
import random
import re
import mimetypes
from .models import ActivationProfile
from django.conf import settings
from boto.s3.connection import S3Connection
from boto.s3.key import Key
import requests
import json
from urllib import parse

SHA1_RE = re.compile('^[a-f0-9]{40}$')
DEFAULT_SETTINGS = {'REGISTRATION_API_ACCOUNT_ACTIVATION_DAYS': 7,}
ACTIVATION_URL = "https://www.taggerin.me/admin/#/access/activation/"

def get_settings(key):
    setting = getattr(settings, key, DEFAULT_SETTINGS.get(key, None))
    if setting is None:
        raise ImproperlyConfigured("The %s setting must not be empty." % key)
    return setting

USER_CREATED_RESPONSE_DATA = {
    'activation_days': get_settings('REGISTRATION_API_ACCOUNT_ACTIVATION_DAYS')
    }

def create_activation_profile(user):
    try:
        activation_key = create_activation_key(user)
        registration_profile = ActivationProfile.objects.create(
            user=user, activation_key=activation_key)
        return registration_profile
    except ValidationError as e:
        raise ValidationError(e)
    except ValueError as e:
        raise ValueError(e)
    except Exception as e:
        raise Exception(e)


def create_activation_key(user):
    username = getattr(user, user.USERNAME_FIELD)
    salt_bytes = str(random.random()).encode('utf-8')
    salt = hashlib.sha1(salt_bytes).hexdigest()[:5]

    hash_input = (salt + username).encode('utf-8')
    activation_key = hashlib.sha1(hash_input).hexdigest()
    return activation_key

def activate_user(activation_key):
    """
    Validate an activation key and activate the corresponding
    ``User`` if valid.
    If the key is valid and has not expired, return the ``User``
    after activating.
    If the key is not valid or has expired, return ``False``.
    If the key is valid but the ``User`` is already active,
    return ``False``.
    To prevent reactivation of an account which has been
    deactivated by site administrators, the activation key is
    reset to the string constant ``RegistrationProfile.ACTIVATED``
    after successful activation.
    """
    # Make sure the key we're trying conforms to the pattern of a
    # SHA1 hash; if it doesn't, no point trying to look it up in
    # the database.
    if SHA1_RE.search(activation_key):
        try:
            profile = ActivationProfile.objects.get(activation_key=activation_key)
            plan = Plan.objects.get(name="PLAN LIGHT")
        except ActivationProfile.DoesNotExist as e:
            print(e)
            return False
        except Exception as e:
            print(e)
            return False
        if not profile.activation_key_expired():
            with transaction.atomic():
                user = profile.user
                user.is_active = True
                user.save()
                profile.activation_key = ActivationProfile.ACTIVATED
                try:
                    Subscription.objects.create(op_subscription="Taggerin", creation_date=arrow.now().isoformat(),
                                                status='Active', period_end_date=datetime.date.today()+datetime.timedelta(days=5000),
                                                charge_date=datetime.date.today(), trial_end_date=datetime.date.today(),
                                                op_plan_id="Taggerin", current_period_number=0, plan_id=plan.id, user_id=profile.user_id)
                except Exception as e:
                    print(e)
                    return False
                profile.save()
                return user
    return False

def send_activation_email(user):
    """
    Send an activation email to the ``user``.
    The activation email will make use of two templates:
    ``registration/activation_email_subject.txt``
    This template will be used for the subject line of the
    email. Because it is used as the subject line of an email,
    this template's output **must** be only a single line of
    text; output longer than one line will be forcibly joined
    into only a single line.
    ``registration/activation_email.txt``
    This template will be used for the body of the email.
    These templates will each receive the following context
    variables:
    ``activation_key``
    The activation key for the new account.
    ``expiration_days``
    The number of days remaining during which the account may
    be activated.
    ``site``
    An object representing the site on which the user
    registered; depending on whether ``django.contrib.sites``
    is installed, this may be an instance of either
    ``django.contrib.sites.models.Site`` (if the sites
    application is installed) or
    ``django.contrib.sites.models.RequestSite`` (if
    not). Consult the documentation for the Django sites
    framework for details regarding these objects' interfaces.
    """

    url = 'http://52.91.69.185/sendemailconfirm'

    ctx_dict = {'activation_key': user.api_registration_profile.activation_key,
                'expiration_days': get_settings('REGISTRATION_API_ACCOUNT_ACTIVATION_DAYS'),
                'site': ACTIVATION_URL}
    subject = render_to_string('registration_api/activation_email_subject.txt', ctx_dict)
    # Email subject *must not* contain newlines
    subject = ''.join(subject.splitlines())
    message = render_to_string('registration_api/activation_email.txt', ctx_dict)
    to = [user.email]
    try:
        r = None
        payload = {'email': to, 'token': user.api_registration_profile.activation_key}
        headers = {'Content-Type': 'application/json'}
        r = requests.post(url, json=payload, headers=headers)
        if(r.status_code != 202):
            r.raise_for_status()
        return ""
        #send_mail(subject, message, settings.EMAIL_HOST_USER, to)
    except ValidationError as e:
        raise ValidationError(e)
    except ValueError as e:
        raise ValueError(e)
    except Exception as e:
        raise Exception(e)


def upload_to_s3(file_obj):
    try:
        a = random.randint(0, 50000)
        b = random.randint(0, 50000)
        c = random.randint(1, 9000)
        file_obj.name= parse.unquote(file_obj.name)
        name_without_spaces = file_obj.name.replace(" ", "_")
        file_obj.name = file_obj.name.replace(" ", "_")
        print(name_without_spaces)
        filename = str(a)+ "-" + str(b+c) + "-"+ name_without_spaces
        conn = S3Connection(settings.AWS_ACCESS_KEY, settings.AWS_SECRET_KEY)
        k = Key(conn.get_bucket(settings.AWS_S3_BUCKET))
        k.key = filename
        file_obj.seek(0)
        k.set_contents_from_file(file_obj)
        url = 'https://s3-us-west-1.amazonaws.com/taggerin-images/' + filename
        return url
    except ValueError:
        error = {"error": ValueError}
        return error
    except Exception as e:
        error = {"error": str(e)}
        return error

    return

def search_tags(list_tags):
    print('search')
    users = None
    try:
        pprint(list_tags)
        requiered_tags = TagOffered.objects.filter(name__in=list_tags)
        #pprint(requiered_tags)
        plan = Plan.objects.get(name="PLAN LIGHT")
        print('plan')
        users = User.objects.filter(user_tagoffered__in=requiered_tags).filter(~Q(User__plan=plan.id))
        print("after")
        pprint(users)
        return list(users)
    except Exception as e:
        print(e)
        return list(users)

def search_tagsRequired(list_tags):
    print('search')
    users = None
    try:
        pprint(list_tags)
        offered_tags = TagOffered.objects.filter(name__in=list_tags)
        #pprint(requiered_tags)
        plan = Plan.objects.get(name="PLAN LIGHT")
        print('plan')
        users = User.objects.filter(user_requiretag__in=offered_tags).filter(~Q(User__plan=plan.id))
        print("after")
        pprint(users)
        return list(users)
    except Exception as e:
        print(e)
        return list(users)

def send_notification_email(users_list):
    #TODO: Enviar notiricaciones a las empresas que este solicitando esos tags
    pass