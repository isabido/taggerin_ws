from django.utils.safestring import mark_safe
from Company.models import Location, ProfilePage, PageImages, Company_Tags_Requiere, Company_Tags_Offered
from users.models import Profile

__author__ = 'isabido'
from pprint import pprint
from django.contrib.gis.geos import Point
from django.db.models import Q, Prefetch
from rest_framework.exceptions import ValidationError
from rest_framework_gis.serializers import GeoFeatureModelSerializer
from payments.models import Subscription
from plans.models import Plan
from users.TaggerinPermissions.TGPermissions import TgPermissions
from django.contrib.gis.measure import D
from rest_framework import serializers
from django.contrib.auth.models import User
import json
from django.core.serializers.json import DjangoJSONEncoder


class Company(serializers.ModelSerializer):
    profile_image = serializers.URLField(required=False)
    category = serializers.IntegerField(required=False)
    password = serializers.CharField(required=False)
    rfc = serializers.CharField(max_length=13, allow_null=True, required=False)
    email = serializers.EmailField(required=False)

    #tagoffered = TagOfferedSerializer(many=True)
    #req_tag = RequestedTagSerializer(many=True)

    class Meta:
        model = User
        fields = ('id','email', 'first_name','password','profile_image', 'category', 'rfc',)
        depth = 2

class PageImagesSerializer(serializers.ModelSerializer):
    #profile_page = ProfilePageSerializer(many=True)
    class Meta:
        model = PageImages
        fields = ('url', 'image_description',)

class ProfilePageSerializer(serializers.ModelSerializer):
    profile_images = PageImagesSerializer(many=True)
    class Meta:
        model = ProfilePage
        fields = ('description', 'web', 'profile_images',)

class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        # geo_field = "point"
        # id_field = False
        fields = ('city', 'state', 'country', 'address','point',)

class GetProfileSerializer(serializers.ModelSerializer):
    profile_image = serializers.URLField(source='user_profile.profile_image')
    category = serializers.CharField(source='user_profile.category.category',max_length=50)
    rfc = serializers.CharField(source='user_profile.rfc', max_length=13)
    location = LocationSerializer(source='user_location', read_only=True)
    tagoffered = serializers.SerializerMethodField('getTagoffered')
    requiredtag = serializers.SerializerMethodField('getRequiretag')
    profile_page = ProfilePageSerializer()

    def getTagoffered(self, obj):
        list_tags = []
        try:
            p = TgPermissions()
            if p.isFreePlan(obj):
                offered = Company_Tags_Offered.objects.filter(Q(profile_id = obj.id), Q(active = True))[:3]
            else:
                offered = Company_Tags_Offered.objects.filter(Q(profile_id = obj.id), Q(active = True))

            for tag in offered:
                model = {
                    "id": tag.tag_id.id,
                    "name": tag.tag_id.name
                }
                list_tags.append(model)

            return list_tags
        except Company_Tags_Requiere.DoesNotExist as e:
            return list_tags
        except ValidationError as e:
            raise  ValidationError(e)
        except ValueError as e:
            raise ValueError(e)
        except Exception as e:
            raise Exception(e)

    def getRequiretag(self, obj):
        list_tags = []
        try:
            p = TgPermissions()
            if p.isFreePlan(obj):
                offered = Company_Tags_Requiere.objects.filter(Q(profile_id = obj.id), Q(active = True))[:3]
            else:
                offered = Company_Tags_Requiere.objects.filter(Q(profile_id = obj.id), Q(active = True))
            for tag in offered:
                model = {
                    "id": tag.tag_id.id,
                    "name": tag.tag_id.name
                }
                list_tags.append(model)
            return list_tags
        except Company_Tags_Requiere.DoesNotExist as e:
            return list_tags
        except ValidationError as e:
            raise  ValidationError(e)
        except ValueError as e:
            raise ValueError(e)
        except Exception as e:
            raise Exception(e)

    class Meta:
        model = User
        fields = ('id','first_name', 'email', 'username', 'location', 'profile_image', 'category', 'rfc', 'tagoffered', 'requiredtag', 'profile_page')


class SearchSerializer(serializers.ModelSerializer):
    profile_image = serializers.URLField(required=False)
    category = serializers.IntegerField(required=False)
    rfc = serializers.CharField(max_length=13, allow_null=True, required=False)
    email = serializers.EmailField(required=False)
    page_profile = ProfilePageSerializer(source='profile_page')
    #tagoffered = TagOfferedSerializer(many=True)
    #req_tag = RequestedTagSerializer(many=True)

    class Meta:
        model = User
        fields = ('id','email', 'first_name','profile_image', 'category', 'rfc', 'page_profile')