from pprint import pprint
from django.contrib.auth.models import User
from django.db.models import Q
from rest_framework import status
from django.http import Http404
from django.shortcuts import render

# Create your views here.
from oauth2_provider.ext.rest_framework import OAuth2Authentication
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.exceptions import ValidationError
from Company.models import Location
from rest_framework import permissions
from plans.models import Plan
from search.serializers import SearchSerializer, GetProfileSerializer
from users.models import Profile
from users.permissions import UserInSessionPermission, UserTypeCompany, UserTypeNormal, BothTypeCompany
from django.contrib.gis.geos import *
from django.contrib.gis.measure import D
import json
from django.core.serializers.json import DjangoJSONEncoder

class SearchLocationViewSet(viewsets.ModelViewSet):

    def get_object(self, pk):
        try:
            obj = User.objects.get(pk=pk)
            #self.check_object_permissions(self.request, obj)
            return obj
        except User.DoesNotExist:
            raise Http404
    authentication_classes = (OAuth2Authentication,)
    #permission_classes = (permissions.IsAuthenticated, UserInSessionPermission, UserTypeCompany,)

    def get_permissions(self):
        return (permissions.IsAuthenticated(), UserInSessionPermission(), BothTypeCompany(),)

    def retrieve(self, request, pk=None):
        try:
            if request.user and request.auth:
                location = User.objects.get(pk=request.user.pk)
            else:
                return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)

            #_data = SearchSerializer(request.query_params)
            #serialized_q = json.dumps(list(_data.data), cls=DjangoJSONEncoder)

            #user = User.objects.filter(pk=request.user.pk).filter(user_location__point__distance_lte=(origin, D(m=distance_m))).distance(origin).order_by('distance').select_related()
            _diameter = int(request.query_params['diameter'])
            _latitude = float(request.query_params['latitude'])
            _longitude = float(request.query_params['longitude'])
            offset = int(request.query_params['offset'])
            limit = int(request.query_params['limit'])
            search = str(request.query_params['search'])
            origin = Point(_latitude, _longitude)
            closest_spot = Location.objects.filter(point__distance_lte=(origin, D(m=_diameter))).distance(origin).order_by('distance')
            plan = Plan.objects.get(name="PLAN LIGHT")

            #Primero obtiene a las empresas que tienen un plan de pago
            #La busqueda se realiza en el nombre de la empresa, los tags y la descripción
            users = User.objects.filter(id__in=closest_spot).filter(~Q(payments_user__plan=plan.id)).filter(Q(user_tagoffered__name__icontains=search) | Q(first_name__icontains=search) | Q(user_requiretag__name__icontains=search) | Q(profile_page__description__icontains=search)).order_by('first_name').distinct()[offset:limit]
            #Si las empresas que tienen realizado el pago no son la cantidad que se debe regresar se añexan todas
            if len(users) < limit:
                users = User.objects.filter(Q(user_tagoffered__name__icontains=search) | Q(first_name__icontains=search) | Q(user_requiretag__name__icontains=search) | Q(profile_page__description__icontains=search)).filter(id__in=closest_spot).order_by('first_name').distinct()[offset:limit]
            #pprint(vars(users))
            #profiles = Profile.objects.filter(user_id__in=users).values()
            #pprint(vars(user))
            # pprint (vars(user[0].user_profile))
            # print(user[0].user_tagoffered)
            #closest_spot = Location.objects.filter(point__distance_lte=(origin, D(m=distance_m))).distance(origin).order_by('distance')[:1][0]
            #closest_spot = Location.objects.filter(point__distance_lte=(origin, D(m=distance_m))).distance(origin).order_by('distance')
            _data = GetProfileSerializer(users, many=True)
            if len(_data.data) < 1:
                return Response({'error': 'No se encontraron empresas cercanas a tu ubicación'}, status=status.HTTP_204_NO_CONTENT)
            #pprint(vars(closest_spot[0].user.user_profile))
            return Response(_data.data, status=status.HTTP_200_OK)
        except ValidationError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print("HHH")
            return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)