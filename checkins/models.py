from django.db import models

# Create your models here.
from django.contrib.auth.models import User
from django.contrib.gis.db import models

class CheckIn(models.Model):
    user = models.ForeignKey(User, related_name="check_in_user")
    createdAt = models.DateTimeField(auto_now_add=True)
    company = models.ForeignKey(User, related_name="company_user")
    check = models.BooleanField(null=False, default=True)
