# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='CheckIn',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('createdAt', models.DateTimeField(auto_now_add=True)),
                ('company', models.ForeignKey(related_name='company_user', to=settings.AUTH_USER_MODEL)),
                ('user', models.ForeignKey(related_name='check_in_user', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
