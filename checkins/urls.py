__author__ = 'isabido'
from .views import CheckinViewSet

CheckinView = CheckinViewSet.as_view({'post': 'create', 'put': 'update', 'get':'retrieve'})
