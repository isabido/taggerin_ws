from django.shortcuts import render

# Create your views here.
from pprint import pprint
from django.contrib.auth.models import User
from django.db.models import Q, Count
from rest_framework import status
from django.http import Http404
from django.shortcuts import render

# Create your views here.
from oauth2_provider.ext.rest_framework import OAuth2Authentication
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.exceptions import ValidationError
from Company.models import Location, Notification
from rest_framework import permissions
from checkins.models import CheckIn
from plans.models import Plan
from search.serializers import SearchSerializer, GetProfileSerializer
from users.models import Profile
from users.permissions import UserInSessionPermission, UserTypeCompany, UserTypeNormal, BothTypeCompany
from django.contrib.gis.geos import *
from django.contrib.gis.measure import D


class CheckinViewSet(viewsets.ModelViewSet):

    def get_object(self, pk):
        try:
            obj = User.objects.get(pk=pk)
            #self.check_object_permissions(self.request, obj)
            return obj
        except User.DoesNotExist:
            raise Http404
    authentication_classes = (OAuth2Authentication,)
    #permission_classes = (permissions.IsAuthenticated, UserInSessionPermission, UserTypeCompany,)

    def get_permissions(self):
        return (permissions.IsAuthenticated(), UserInSessionPermission(), BothTypeCompany(),)



    def create(self, request):
        try:
            if request.user and request.auth:
                user = User.objects.get(pk=request.user.pk)
            else:
                return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)

            check = bool(request.data['check'])
            company = int(request.data['company'])
            company_band = User.objects.get(pk=company)
            print(check)
            if company_band and company != user.id:
                updated_values = {'check': check}
                obj, created = CheckIn.objects.update_or_create(user_id=user.id, company_id=company, defaults=updated_values)
                if check and created:
                    description = "El usuario {0} ha realizado un checkin en su empresa.".format(request.user.id)
                    Notification.objects.create(user_id=company, notification_type=2, description=description, match=int(request.user.id))
                return Response({}, status=status.HTTP_204_NO_CONTENT)
            else:
                return Response({'error': 'La empresa no existe'}, status=status.HTTP_412_PRECONDITION_FAILED)


        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)

    def update(self, request):
        try:
            if request.user and request.auth:
                user = User.objects.get(pk=request.user.pk)
            else:
                return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)

            check = bool(request.data['check'])
            company = int(request.data['company'])
            company_band = User.objects.get(pk=company)
            print(check)
            if company_band and company != user.id:
                updated_values = {'check': check}
                if check:
                    actual_check = False
                else:
                    actual_check = True
                obj, created = CheckIn.objects.update_or_create(user_id=user.id, company_id=company, defaults=updated_values)
                return Response({}, status=status.HTTP_204_NO_CONTENT)
            else:
                return Response({'error': 'La empresa no existe'}, status=status.HTTP_412_PRECONDITION_FAILED)


        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)

    def retrieve(self, request, pk=None):
        try:
            if request.user and request.auth:
                user = User.objects.get(pk=request.user.pk)
            else:
                return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)
            if pk == None:
                return Response({'error': "Debe enviar el identificador de la empresa"}, status=status.HTTP_400_BAD_REQUEST)
            checkins = CheckIn.objects.filter(company_id=pk, check=True).aggregate(checkin_count=Count('user_id'))
            is_check = CheckIn.objects.filter(company_id=pk,user_id=user.id).values()
            if len(is_check) > 0:
                check_info = {
                'check': is_check[0]['check'],
                'date': is_check[0]['createdAt']
                }
            else:
                check_info = {
                'check': False,
                'date': None
                }

            resp = {
                'is_check':check_info,
                'total_checkins': checkins
            }

            return Response(resp, status=status.HTTP_200_OK)
        except ValidationError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print("HHH")
            return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)