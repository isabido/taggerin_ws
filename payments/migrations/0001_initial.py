# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('plans', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomerAddress',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('city', models.CharField(max_length=100)),
                ('state', models.CharField(max_length=100)),
                ('line1', models.CharField(max_length=100)),
                ('postal_code', models.CharField(max_length=15)),
                ('line2', models.CharField(max_length=100)),
                ('country_code', models.CharField(max_length=3)),
            ],
        ),
        migrations.CreateModel(
            name='CustomerCreditCard',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('credit_card', models.CharField(max_length=100)),
                ('holder_name', models.CharField(max_length=150)),
            ],
        ),
        migrations.CreateModel(
            name='OpenPayCustomer',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=150)),
                ('customer_id', models.CharField(max_length=100)),
                ('subscription_id', models.CharField(max_length=100)),
                ('last_name', models.CharField(max_length=60)),
                ('phone', models.CharField(max_length=20)),
                ('user', models.OneToOneField(related_name='op_customer_user', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Payments',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('duration', models.IntegerField()),
                ('dateOfpurchase', models.DateTimeField()),
                ('trial_end_date', models.DateField()),
                ('period_end_date', models.DateField()),
                ('amount', models.DecimalField(max_digits=10, decimal_places=2)),
                ('creation_date', models.DateTimeField()),
                ('op_subscription', models.CharField(max_length=50)),
                ('plan', models.ForeignKey(related_name='payments_plan', to='plans.Plan')),
                ('user', models.ForeignKey(related_name='payments_user', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Subscription',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('op_subscription', models.CharField(max_length=50)),
                ('creation_date', models.DateTimeField()),
                ('status', models.CharField(max_length=50)),
                ('period_end_date', models.DateField()),
                ('charge_date', models.DateField()),
                ('trial_end_date', models.DateField()),
                ('op_plan_id', models.CharField(max_length=100)),
                ('current_period_number', models.IntegerField()),
                ('plan', models.ForeignKey(related_name='subscription_plan', to='plans.Plan')),
                ('user', models.OneToOneField(related_name='User', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='customercreditcard',
            name='customer',
            field=models.ForeignKey(related_name='customer_openpay', to='payments.OpenPayCustomer'),
        ),
        migrations.AddField(
            model_name='customeraddress',
            name='customer',
            field=models.ForeignKey(related_name='customer_address_customer', to='payments.OpenPayCustomer'),
        ),
    ]
