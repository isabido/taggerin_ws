from pprint import pprint
from django.core.exceptions import FieldDoesNotExist, FieldError, ValidationError
from django.db import transaction, IntegrityError, Error
from django.db.transaction import TransactionManagementError
from rest_framework import serializers
from payments.PaymentAdapter import PaymentAdapter
from plans.models import Plan, Type

__author__ = 'isabido'

from .models import OpenPayCustomer, CustomerCreditCard, Subscription, CustomerAddress, Payments


class Customer(object):

    def __init__(self):
        self.customer = None
        self.creditCard = None
        self.openPay = PaymentAdapter()
        self.subscription = None
        self.address = None


    def getCustomerDict(self, model, user):
        customer_dict = {"name": model['name'], "customer_id": model['customer_id'], "last_name": "",
                         "subscription_id": model['subscription']['id'], "user_id": user.id,
                         "phone": model['phone']
                         }
        return customer_dict

    def getCreditCardDict(self, model, customer):
        creditCard_dict = {"credit_card": model['creditCard']['op_creditCard_id'],
                           "holder_name": model['name'],
                           "customer_id": customer.id
                           }

        return creditCard_dict

    def getSubscriptionDict(self, model, user, tg_plan):
        try:
            subscription_dict = {
                "op_subscription": model['subscription']['id'], "creation_date": model['subscription']['creation_date'],
                "status":model['subscription']['status'], "period_end_date": model['subscription']['period_end_date'],
                "charge_date": model['subscription']['charge_date'], "trial_end_date": model['subscription']['trial_end_date'],
                "op_plan_id": model['subscription']['plan_id'],"current_period_number":model['subscription']['current_period_number'],
                "plan_id": tg_plan
            }
        except TypeError as e:
            raise TypeError({'error':e})
        return subscription_dict


    def getAddressDict(self, model, user):
        try:
            subscription_dict = {
                "city": model['address']['city'],
                "state":model['address']['state'],
                "line1": model['address']['line1'],
                "line2": model['address']['line2'],
                "country_code": "MX",
                "postal_code":model['address']['postal_code'],
                "customer_id": user
            }

        except TypeError as e:
            raise TypeError({'error':e})
        return subscription_dict

    def saveCustomer(self, model, user, tg_plan):
        with transaction.atomic():
            try:
                pprint(self.getCustomerDict(model, user))
                print("opc")
                self.customer = OpenPayCustomer.objects.create(**self.getCustomerDict(model, user))
                print("credcard")
                #self.creditCard= CustomerCreditCard.objects.create(**self.getCreditCardDict(model, self.customer))
                print("subscription")
                print("user id " + str(user.id))
                print(self.getSubscriptionDict(model, user.id, tg_plan))
                self.subscription= Subscription.objects.filter(user_id=user.id).update(**self.getSubscriptionDict(model, user.id, tg_plan))
                #TODO: falta guardar la dirección
                self.address = CustomerAddress.objects.create(**self.getAddressDict(model, self.customer.id))
                self.saveHistory(model, user, tg_plan)
                print("fint sub")
                return True
            except ValueError as e:
                print(e)
                return False
            except IntegrityError as e:
                print(e)
                return False
            except FieldDoesNotExist as e:
                print(e)
                return False
            except FieldError as e:
                print(e)
                return False
            except ValidationError as e:
                print(e)
                return False
            except TransactionManagementError as e:
                print(e)
                return False
            except TypeError as e:
                print(e)
                return False
            except Error as e:
                print(e)
                return False

    def saveHistory(self, model, user, tg_plan):
        try:
            plan = Type.objects.get(plan_id=tg_plan)
            payment_model = {
                "user": user,
                "plan_id": tg_plan,
                "duration": plan.repeat_every,
                "dateOfpurchase": model['subscription']['creation_date'],
                "trial_end_date":model['subscription']['trial_end_date'],
                "period_end_date": model['subscription']['period_end_date'],
                "creation_date": model['subscription']['creation_date'],
                "op_subscription": model['subscription']['id'],
                "amount": plan.amount

            }
            Payments.objects.create(**payment_model)
        except Exception as e:
            print(e)
            return False



    def updateCustomer(self, model, user, tg_plan):
        with transaction.atomic():
            try:
                Subscription.objects.filter(user_id=user.id).update(**self.getSubscriptionDict(model, user.id, tg_plan))
                print("opc")

                self.customer = OpenPayCustomer.objects.filter(id=user.id).update(**self.getCustomerDict(model, user))
                print("credcard")
                #self.creditCard= CustomerCreditCard.objects.create(**self.getCreditCardDict(model, self.customer))
                print("fint sub")
                self.saveHistory(model, user, tg_plan)
                return True
            except ValueError as e:
                print(e)
                return False
            except IntegrityError as e:
                print(e)
                return False
            except FieldDoesNotExist as e:
                print(e)
                return False
            except FieldError as e:
                print(e)
                return False
            except ValidationError as e:
                print(e)
                return False
            except TransactionManagementError as e:
                print(e)
                return False
            except TypeError as e:
                print(e)
                return False
            except Error as e:
                print(e)
                return False

    def getCustomer(self, user):
         try:
             print("get customer")
             self.customer = OpenPayCustomer.objects.get(user_id=user.id)
             #print("customer " + self.customer.id)
             op_customer = None
             if self.customer is not None:
                op_customer = self.openPay.getCustomer(self.customer.customer_id)
                if op_customer['id'] is None:
                     op_customer = None
                else:
                    print("not none")
             return op_customer, self.customer
         except ValueError as e:
             return None, None
         except  OpenPayCustomer.DoesNotExist as e:
             return None, None
         except Exception as e:
             return None, None


    def getCreditCard(self, customer):
        try:
            self.creditCard = CustomerCreditCard.objects.get(customer_id=customer)
            return self.creditCard
        except ValueError as e:
             return None
        except  OpenPayCustomer.DoesNotExist as e:
             return None

