from django.db import models
from django.contrib.auth.models import User
# Create your models here.
from plans.models import Plan


class Payments(models.Model):
    user = models.ForeignKey(User, related_name="payments_user")
    plan = models.ForeignKey(Plan, related_name="payments_plan")
    duration = models.IntegerField()
    dateOfpurchase = models.DateTimeField()
    trial_end_date = models.DateField(auto_now=False)
    period_end_date = models.DateField()
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    creation_date = models.DateTimeField(auto_now=False)
    op_subscription = models.CharField(max_length=50)

class OpenPayCustomer(models.Model):
    user = models.OneToOneField(User, related_name="op_customer_user")
    name = models.CharField(max_length=150, null=False)
    customer_id = models.CharField(max_length=100)
    subscription_id = models.CharField(max_length=100)
    last_name = models.CharField(max_length=60)
    phone = models.CharField(max_length=20)


class CustomerAddress(models.Model):
    customer = models.ForeignKey(OpenPayCustomer, related_name="customer_address_customer")
    city = models.CharField(max_length=100, null=False)
    state = models.CharField(max_length=100, null=False)
    line1 = models.CharField(max_length=100, null=False)
    postal_code = models.CharField(max_length=15)
    line2 = models.CharField(max_length=100)
    country_code = models.CharField(max_length=3)

class CustomerCreditCard(models.Model):
    customer = models.ForeignKey(OpenPayCustomer, related_name="customer_openpay")
    credit_card = models.CharField(max_length=100)
    holder_name = models.CharField(max_length=150, null=False)

class Subscription(models.Model):
    plan = models.ForeignKey(Plan, related_name="subscription_plan")
    user = models.OneToOneField(User, related_name="User")
    op_subscription = models.CharField(max_length=50)
    creation_date = models.DateTimeField(auto_now=False)
    status = models.CharField(max_length=50)
    period_end_date = models.DateField(auto_now=False)
    charge_date = models.DateField(auto_now=False)
    trial_end_date = models.DateField(auto_now=False)
    op_plan_id = models.CharField(max_length=100)
    current_period_number = models.IntegerField()
