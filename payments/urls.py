__author__ = 'isabido'
from .views import CreateCustomerViewSet, CreateSubscriptionViewSet

CreateCustomerView = CreateCustomerViewSet.as_view({'post': 'create', 'get':'retrieve'})

SubscriptionView = CreateSubscriptionViewSet.as_view({'get': 'retrieve'})