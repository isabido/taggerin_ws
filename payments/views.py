from django.db.models import Q

from rest_framework import status
from django.contrib.auth.models import User
from django.http import Http404
from django.shortcuts import render
from oauth2_provider.ext.rest_framework import OAuth2Authentication
import openpay
# Create your views here.
from rest_framework import viewsets
from payments.Customer import Customer
from payments.PaymentAdapter import PaymentAdapter
from payments.models import Subscription
from payments.serializers import getSubscription
from plans.models import Plan, Type

from users.permissions import UserInSessionPermission, UserTypeCompany
from rest_framework.permissions import AllowAny
from rest_framework import permissions
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from pprint import pprint

openpay.api_key = "sk_10d37cc4da8e4ffd902cdf62e37abd1b"
openpay.verify_ssl_certs = False
openpay.merchant_id = "mynvbjhtzxdyfewlzmdo"
openpay.production = False  # By default this works in sandbox mode

class CreateCustomerViewSet(viewsets.ViewSet):


    def get_object(self, pk):
        try:
            obj = User.objects.get(pk=pk)
            #self.check_object_permissions(self.request, obj)
            return obj
        except User.DoesNotExist:
            raise Http404
    authentication_classes = (OAuth2Authentication,)

    def get_permissions(self):
        return (permissions.IsAuthenticated(), UserInSessionPermission(), UserTypeCompany(),)

    def retrieve(self, request, pk=None):
        if request.user and request.auth:
            user = self.get_object(request.user.pk)
        else:
            return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)

        openpay = PaymentAdapter()

        openpay.getCustomer()
        # user = User.objects.filter(pk=request.user.pk).select_related()
        #serializer = GetCompanySerializer(user)
        return Response({"data":5}, status=status.HTTP_200_OK)

    def create(self, request):
        if request.user and request.auth:
            user = self.get_object(request.user.pk)
        else:
            return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)
        openpay = PaymentAdapter()
        customerBd = Customer()
        cust_band = False
        #print(request.data['address']['city'])
        request.data['email'] = request.user.username
        tg_plan = request.data['subscription']['plan_id']


        try:#TODO: validad fecha o suscripción. Validad tipo de usuario
            customer, tg_customer = customerBd.getCustomer(request.user)
            subs = Subscription.objects.get(user_id = request.user.id)
            #pprint(vars(subs))
            print("before type: " + str(request.user.id))

            #pprint(vars(customer))
            op_plan_id = Type.objects.get(plan_id=tg_plan)
            print(op_plan_id.plan.name)
            print("type " + op_plan_id.openpay_id)
            print("subs " + subs.plan.name)
            if customer == None and tg_customer==None and subs.plan.name == "PLAN LIGHT":
                print("NONE no existe")
                op_customer = openpay.createCustomer(request.data)
                #op_creditCard = openpay.createCreditCard(request.data)


                print(op_plan_id.id)
                #subscription = openpay.createSubscription2(op_plan_id.openpay_id, op_creditCard['id'])
                subscription = openpay.createSubscription2(op_plan_id.openpay_id, request.data)

                model = request.data
                model['customer_id'] = op_customer['id']
                #model['creditCard']['op_creditCard_id'] = op_creditCard['id']
                subscription.pop("card", None)
                model['creditCard'].pop("card_number", None)
                model['creditCard'].pop("cvv2", None)
                model['creditCard'].pop("expiration_year", None)
                model['creditCard'].pop("expiration_month", None)
                subscription.pop("customer_id", None)
                model['subscription'] = subscription
                #TODO: crear excepciones propias
                #Se guardan los datos obtenidos de openpay
                cust_band = customerBd.saveCustomer(model, user, tg_plan)
                print(cust_band)
                if(cust_band):
                    return Response(subscription, status=status.HTTP_200_OK)

            elif subs.plan.name != "PLAN LIGHT" and op_plan_id.plan.name != "PLAN LIGHT" and subs.plan_id == tg_plan:
                print("NONE si existe")
                openpay.setCUstomer(customer)
                #subs = Subscription.objects.get(user_id = user.id)
                #op_customer = openpay.getCustomer(request.data)
                #op_creditCard = openpay.createCreditCard(request.data)
                print("get plan")
                op_plan_id = Type.objects.get(plan_id=tg_plan)
                print("update subsctip")
                subscription = openpay.updateSubscription(op_plan_id, request.data, subs.op_subscription)
                print(subscription)
                print("begin model")
                model = request.data
                model['customer_id'] = customer['id']
                #model['creditCard']['op_creditCard_id'] = op_creditCard['id']
                subscription.pop("card", None)
                model['creditCard'].pop("card_number", None)
                model['creditCard'].pop("cvv2", None)
                model['creditCard'].pop("expiration_year", None)
                model['creditCard'].pop("expiration_month", None)
                #subscription.pop("customer_id", None)
                model['subscription'] = subscription
                cust_band = customerBd.updateCustomer(model, user, tg_plan)
                print(cust_band)
                if(cust_band):
                    return Response(subscription, status=status.HTTP_200_OK)
            elif subs.plan_id != tg_plan and op_plan_id.plan.name != "PLAN LIGHT":
                openpay.setCUstomer(customer)
                op_plan_id = Type.objects.get(plan_id=tg_plan)
                cancel = openpay.cancelSubscription(subs.op_subscription)
                print("cancel")
                print(cancel)
                subscription = openpay.createSubscription2(op_plan_id.openpay_id, request.data)
                model = request.data
                model['customer_id'] = customer['id']
                #model['creditCard']['op_creditCard_id'] = op_creditCard['id']
                subscription.pop("card", None)
                model['creditCard'].pop("card_number", None)
                model['creditCard'].pop("cvv2", None)
                model['creditCard'].pop("expiration_year", None)
                model['creditCard'].pop("expiration_month", None)
                #subscription.pop("customer_id", None)
                model['subscription'] = subscription
                cust_band = customerBd.updateCustomer(model, user, tg_plan)
                print(cust_band)
                if(cust_band):
                    return Response(subscription, status=status.HTTP_200_OK)


        except ValueError as e:
            return Response(e, status=status.HTTP_406_NOT_ACCEPTABLE)
        except TypeError as e:
            return Response({"error": "Some data is invalid"}, status=status.HTTP_406_NOT_ACCEPTABLE)
        except Exception as e:
            return Response({"error": str(e)}, status=status.HTTP_406_NOT_ACCEPTABLE)
        #pprint (vars(customer))
        #print(creditCard)


        return Response({'error': "No se pudo realizar la suscripción"}, status=status.HTTP_400_BAD_REQUEST)

class CreateSubscriptionViewSet(viewsets.ViewSet):


    def get_object(self, pk):
        try:
            obj = User.objects.get(pk=pk)
            #self.check_object_permissions(self.request, obj)
            return obj
        except User.DoesNotExist:
            raise Http404
    authentication_classes = (OAuth2Authentication,)

    def get_permissions(self):
        return (permissions.IsAuthenticated(), UserInSessionPermission(), UserTypeCompany(),)

    def retrieve(self, request, pk=None):
        try:
            if request.user and request.auth:
                user = self.get_object(request.user.pk)
            else:
                return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)
            subscription = Subscription.objects.get(user_id = user.id)

            serializer = getSubscription(subscription)
            # openpay = PaymentAdapter()
            # openpay.getCustomer()
            # user = User.objects.filter(pk=request.user.pk).select_related()
            #serializer = GetCompanySerializer(user)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except ValueError as e:
            return Response(e, status=status.HTTP_406_NOT_ACCEPTABLE)
        except TypeError as e:
            return Response({"error": "Some data is invalid"}, status=status.HTTP_406_NOT_ACCEPTABLE)
        except Exception as e:
            return Response({"error": str(e)}, status=status.HTTP_406_NOT_ACCEPTABLE)

    def create(self, request):
        try:
            if request.user and request.auth:
                user = self.get_object(request.user.pk)
            else:
                return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)
            openpay = PaymentAdapter()
            print(request.data['address']['city'])
            request.data['email'] = request.user.username
            try:
                customer = openpay.createCustomer(request.data)
                creditCard = openpay.createCreditCard(request.data)
            except ValueError as e:
                return Response(e, status=status.HTTP_406_NOT_ACCEPTABLE)
            #pprint (vars(customer))
            #print(creditCard)
            model = request.data
            model['customer_id'] = customer['id']
            model['creditCard']['creditCard_id'] = creditCard['id']

            return Response(model, status=status.HTTP_200_OK)
        except ValueError as e:
            return Response(e, status=status.HTTP_406_NOT_ACCEPTABLE)
        except TypeError as e:
            return Response({"error": "Some data is invalid"}, status=status.HTTP_406_NOT_ACCEPTABLE)
        except Exception as e:
            return Response({"error": str(e)}, status=status.HTTP_406_NOT_ACCEPTABLE)


class GetSubscriptionViewSet(viewsets.ViewSet):


    def get_object(self, pk):
        try:
            obj = User.objects.get(pk=pk)
            #self.check_object_permissions(self.request, obj)
            return obj
        except User.DoesNotExist:
            raise Http404
    authentication_classes = (OAuth2Authentication,)

    def get_permissions(self):
        return (permissions.IsAuthenticated(), UserInSessionPermission(), UserTypeCompany(),)

    def retrieve(self, request, pk=None):
        if request.user and request.auth:
            user = self.get_object(request.user.pk)
        else:
            return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)

        openpay = PaymentAdapter()
        openpay.getCustomer()
        # user = User.objects.filter(pk=request.user.pk).select_related()
        #serializer = GetCompanySerializer(user)
        return Response({"data":5}, status=status.HTTP_200_OK)

