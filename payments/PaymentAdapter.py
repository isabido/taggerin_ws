import datetime
from rest_framework import serializers

__author__ = 'isabido'
import openpay

class PaymentAdapter(object):

    def __init__(self):
        openpay.api_key = "sk_9091a35de8c445ae9f89e1ea980b9a33"
        openpay.verify_ssl_certs = False
        openpay.merchant_id = "m536yk8ljrwtsctrnzk1"
        openpay.production = False

        self.customer = None
        self.creditCard = None
        self.charge = None
        self.subscription = None
        self.plan = None
        self.plans = None


    def createCustomer(self, request):
        from piston_mini_client import APIError
        try:

            if self.customer is None:
                self.customer = openpay.Customer.create(
                    name=request['name'],
                    email=request['email'],
                    address= request['address'],
                    # {
                    #     "city": request['address']['city'],
                    #     "state":request['address']['state'],
                    #     "line1": request['address']['street'],
                    #     "postal_code":request['address']['postal_code'],
                    #     "line2":request['address']['line2'],
                    #     "line3":request['address']['line3'],
                    #     "country_code":request['address']['country_code']
                    # },
                    #last_name=request['last_name'],
                    phone_number=request['phone']
                )
        except openpay.APIError as e:
            print("Error")
            raise serializers.ValidationError({'error':e})
        except openpay.InvalidRequestError as e:
            print("Error")
            raise serializers.ValidationError({'error':e})

        return self.customer

    def getCustomer(self, customer_id):
        try:
            self.customer = openpay.Customer.retrieve(customer_id)

        except openpay.APIError as e:
            print("Error")
            raise serializers.ValidationError({'error':e})
        except openpay.InvalidRequestError as e:
            print("Error")
            raise serializers.ValidationError({'error':e})
        return self.customer

    def setCUstomer(self, customer):
        self.customer = customer

    def getAllCustomers(self):
        customers = openpay.Customer.all()
        return customers

    def getCustomerCard(self, card_id):
        try:
            self.creditCard = self.customer.cards.retrieve(card_id)
        except openpay.APIError as e:
            print("Error")
            raise serializers.ValidationError({'error':e})
        except openpay.InvalidRequestError as e:
            print("Error")
            raise serializers.ValidationError({'error':e})
        return self.creditCard

    def createCreditCard(self, request):
        try:
            self.creditCard = self.customer.cards.create(
            card_number=request['creditCard']['card_number'],
            holder_name=request['name'],
            expiration_year=request['creditCard']['expiration_year'],
            expiration_month=request['creditCard']['expiration_month'],
            cvv2=request['creditCard']['cvv2'],
            address= request['address'])
        except openpay.APIError as e:
            print("Error")
            raise serializers.ValidationError({'error':e})
        except openpay.InvalidRequestError as e:
            print("Error")
            print(e)
            raise serializers.ValidationError({'error':e})
        except openpay.CardError as e:
            print(e)
            raise serializers.ValidationError({'error':e})
        return self.creditCard

    def createPlan(self, request):
        try:
            self.plan = openpay.Plan.create(
                amount=request['amount'],
                status_after_retry=request['status_after_retry'],
                retry_times=request['retry_times'],
                name=request['name'],
                repeat_unit=request['repeat_unit'],
                trial_days=request['trial_days'],
                repeat_every=request['repeat_every']
            )
        except openpay.APIError as e:
            print("Error")
            raise serializers.ValidationError({'error':e})
        except openpay.InvalidRequestError as e:
            print("Error")
            print(e)
            raise serializers.ValidationError({'error':e})
        except openpay.CardError as e:
            print(e)
            raise serializers.ValidationError({'error':e})

        return self.plan

    def getPlan(self, id):
        try:
            self.plan = openpay.Plan.retrieve(id)
        except openpay.APIError as e:
            print("Error")
            raise serializers.ValidationError({'error':e})
        except openpay.InvalidRequestError as e:
            print("Error")
            print(e)
            raise serializers.ValidationError({'error':e})
        except openpay.CardError as e:
            print(e)
            raise serializers.ValidationError({'error':e})
        return self.plan

    def getListPlans(self):
        try:
            params = {"limit":3}
            self.plans = openpay.Plan.all()
        except openpay.APIError as e:
            print("Error")
            raise serializers.ValidationError({'error':e})
        except openpay.InvalidRequestError as e:
            print("Error")
            print(e)
            raise serializers.ValidationError({'error':e})
        except openpay.CardError as e:
            print(e)
            raise serializers.ValidationError({'error':e})
        return self.plans


    def createSubscription(self, plan, card):
        try:
            self.subscription = self.customer.subscriptions.create(
                plan_id=plan,
                trial_days="60",
                card_id= card
            )
        except openpay.APIError as e:
            print("Error")
            raise serializers.ValidationError({'error':e})
        except openpay.InvalidRequestError as e:
            print("Error")
            print(e)
            raise serializers.ValidationError({'error':e})
        except openpay.CardError as e:
            print(e)
            raise serializers.ValidationError({'error':e})
        return self.subscription

    def createSubscription2(self, plan, card):
        try:
            model = {

                  'card_number':card['creditCard']['card_number'],
                  'holder_name':card['name'],
                  'expiration_year':card['creditCard']['expiration_year'],
                  'expiration_month':card['creditCard']['expiration_month'],
                  'cvv2':card['creditCard']['cvv2']

            }

            self.subscription = self.customer.subscriptions.create(
                plan_id=plan,
                trial_days="60",
                card= model
            )
        except openpay.APIError as e:
            print("Error")
            raise serializers.ValidationError({'error':e})
        except openpay.InvalidRequestError as e:
            print("Error")
            print(e)
            raise serializers.ValidationError({'error':e})
        except openpay.CardError as e:
            print(e)
            raise serializers.ValidationError({'error':e})
        return self.subscription

    def cancelSubscription(self, op_subs_id):
        try:
            self.subscription = self.customer.subscriptions.retrieve(op_subs_id)
            self.subscription.delete()
        except openpay.APIError as e:
            print("Error")
            raise serializers.ValidationError({'error':e})
        except openpay.InvalidRequestError as e:
            print("Error")
            print(e)
            raise serializers.ValidationError({'error':e})
        except openpay.CardError as e:
            print(e)
            raise serializers.ValidationError({'error':e})
        except Exception as e:
            raise Exception(e)
        return self.subscription

    def updateSubscription(self, plan, card, subs):
        print("en update subsc " + subs)
        try:
            date = datetime.date.today()-datetime.timedelta(days=1)
            model = {

                  'card_number':card['creditCard']['card_number'],
                  'holder_name':card['name'],
                  'expiration_year':card['creditCard']['expiration_year'],
                  'expiration_month':card['creditCard']['expiration_month'],
                  'cvv2':card['creditCard']['cvv2']

            }
            print("before openpay update subsc")
            # self.subscription = self.customer.subscriptions.update(
            #     cancel_at_period_end=True,
            #     trial_end_date=date,
            #     card= model
            # )
            #print(self.customer)
            self.subscription = self.customer.subscriptions.retrieve(subs)
            print("list")
            self.subscription.cancel_at_end_period = True
            self.subscription.card = model
            #self.subscription.trial_end_date=date.strftime('%Y/%m/%d')
            print("before op update subsc save")
            self.subscription.save()
            print("after op update subsc")
        except openpay.InvalidRequestError as e:
            print(e)
            raise serializers.ValidationError({'error':e})
        except openpay.CardError as e:
            print(e)
            raise serializers.ValidationError({'error':e})
        except openpay.AuthenticationError as e:
            print(e)
            raise serializers.ValidationError({'error':e})
        except openpay.APIError as e:
            print("Error")
            raise serializers.ValidationError({'error':e})
        except openpay.InvalidRequestError as e:
            print("Error")
            print(e)
            raise serializers.ValidationError({'error':e})
        except openpay.CardError as e:
            print(e)
            raise serializers.ValidationError({'error':e})
        except Exception as e:
            print(e)
            raise serializers.ValidationError({'error':e})
        return self.subscription

    def getSubscription(self, id):
        print(id)
        self.subscription = self.customer.subscriptions.retrieve(id)
        return self.subscription


