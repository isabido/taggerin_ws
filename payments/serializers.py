from rest_framework import serializers
from payments.models import Subscription
from plans.models import Plan, Type

__author__ = 'isabido'


class getPlanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Plan
        fields = ('id', 'name',)

class getSubscription(serializers.Serializer):
    id = serializers.IntegerField()
    creation_date = serializers.DateTimeField()
    period_end_date = serializers.DateField()
    plan = getPlanSerializer()

