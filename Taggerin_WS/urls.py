"""Taggerin_WS URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from Company.urls import CompanyView, TagsView, LocationView, UploadProfileImageCompany, ProfilePageCompany, CategoryPageCompany, \
    PublicProfilePageCompany
from normaluser.urls import NormalView
from payments.urls import CreateCustomerView, SubscriptionView
from plans.urls import CreatePlanView, ListPlansView
from search.urls import ListLocationsView
from users.urls import NormalUser_create
from activations.urls import activationView
from conversations.urls import ConversationsUser, Message, List_Conversations
from notifications.urls import NotificationView
from checkins.urls import CheckinView
from ratings.urls import RatingView


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^oauth/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    url(r'^company/?$', CompanyView, name='company'),
    url(r'^users/?$', NormalUser_create, name='normal-user'),
    url(r'^company/activation/(?P<activation_key>\w+)/?$', activationView, name='activation-company'),
    url(r'^company/tags/?$', TagsView, name='tags-company'),
    url(r'^company/location/?$', LocationView, name='location-company'),
    url(r'^company/profile/image/?$', UploadProfileImageCompany, name='image-company'),
    url(r'^company/profile/page/?$', ProfilePageCompany, name='page-profile-company'),
    url(r'^conversations/?$', List_Conversations, name='users-conversations'),
    url(r'^conversations/(?P<pk>[0-9]+)/messages/?$', ConversationsUser, name='users-conversations'),
    url(r'^conversations/message/?$', Message, name='message'),
    url(r'^company/notifications/?$', NotificationView, name='notifications'),
    url(r'^company/categories/?$', CategoryPageCompany, name='categories'),
    url(r'^payments/customer/?$', CreateCustomerView, name='customer'),
    url(r'^plan/?$', CreatePlanView, name='create_plan'),
    url(r'^plan/(?P<pk>[0-9]+)/?$', CreatePlanView, name='get_plan'),
    url(r'^plans/?$', ListPlansView, name='list_plans'),
    url(r'^subscription/?$', SubscriptionView, name='get_subscription'),
    url(r'^search/locations/?$', ListLocationsView, name='get_locations'),
    url(r'^profile/(?P<pk>[0-9]+)/?$', PublicProfilePageCompany, name='get_public_profile'),
    url(r'^company/checkin/?$', CheckinView, name='checkin'),
    url(r'^company/(?P<pk>[0-9]+)/checkins/?$', CheckinView, name='company_rating'),
    url(r'^company/rating/?$', RatingView, name='company_rating'),
    url(r'^company/(?P<pk>[0-9]+)/rating/?$', RatingView, name='company_rating'),


]
#urlpatterns += router.urls