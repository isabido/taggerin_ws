from django.db.models import Q

from rest_framework import status
from django.contrib.auth.models import User
from django.http import Http404
from django.shortcuts import render
from oauth2_provider.ext.rest_framework import OAuth2Authentication
import openpay
# Create your views here.
from rest_framework import viewsets
from payments.PaymentAdapter import PaymentAdapter
from plans.Plan import TGPlan
from plans.models import Plan, Type
from users.permissions import UserInSessionPermission, UserTypeCompany
from rest_framework.permissions import AllowAny
from rest_framework import permissions
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from pprint import pprint

openpay.api_key = "sk_10d37cc4da8e4ffd902cdf62e37abd1b"
openpay.verify_ssl_certs = False
openpay.merchant_id = "mynvbjhtzxdyfewlzmdo"
openpay.production = False  # By default this works in sandbox mode

class CreatePlanViewSet(viewsets.ViewSet):


    def get_object(self, pk):
        try:
            obj = User.objects.get(pk=pk)
            #self.check_object_permissions(self.request, obj)
            return obj
        except User.DoesNotExist:
            raise Http404
    authentication_classes = (OAuth2Authentication,)

    def get_permissions(self):
        return (permissions.IsAuthenticated(), UserInSessionPermission(), UserTypeCompany(),)

    def retrieve(self, request, pk=None):
        if request.user and request.auth:
            try:
                user = self.get_object(request.user.pk)
            except Exception as e:
                return Response({'error': str(e)}, status=status.HTTP_401_UNAUTHORIZED)
        else:
            return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)
        try:
            openpay = PaymentAdapter()
            plan = Plan.objects.get(pk = pk)
            type_plan = Type.objects.get(plan_id = pk)
            pprint(vars(type_plan))
            id = type_plan.openpay_id
            if plan.name == "PLAN LIGHT":
                dict_plan = {"trial_days": 0,
                      "repeat_unit": "month",
                      "amount": type_plan.amount,
                      "name": plan.name,
                      "creation_date": "2016-01-16T10:36:44-06:00",
                      "status": type_plan.active,
                      "repeat_every": type_plan.repeat_every,
                      "status_after_retry": type_plan.status_after_retry,
                      "id": plan.id,
                      "retry_times": 0,
                      "description": plan.description,
                      "currency": "MXN"}
                return Response(dict_plan, status=status.HTTP_200_OK)
            else:
                op_plan = openpay.getPlan(id)
                dict_plan = {"trial_days": op_plan.trial_days,
                      "repeat_unit": op_plan.repeat_unit,
                      "amount": op_plan.amount,
                      "name": plan.name,
                      "creation_date": op_plan.creation_date,
                      "status": op_plan.status,
                      "repeat_every": op_plan.repeat_every,
                      "status_after_retry": op_plan.status_after_retry,
                      "id": op_plan.id,
                      "retry_times": op_plan.retry_times,
                      "description": plan.description,
                      "currency": op_plan.currency}
                return Response(dict_plan, status=status.HTTP_200_OK)
            # user = User.objects.filter(pk=request.user.pk).select_related()
            #serializer = GetCompanySerializer(user)

        except ValueError as e:
            return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_406_NOT_ACCEPTABLE)

    def list(self, request):
        if request.user and request.auth:
            user = self.get_object(request.user.pk)
        else:
            return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)

        openpay = PaymentAdapter()
        try:
            #plan = openpay.getListPlans()
            plan2 = Plan.objects.all()
            typ =  Type.objects.all()
            dict_plan = {}
            list_plan= []
            for p in plan2:
                typ =  Type.objects.get(plan_id = p.id)
                dict_t = {
                    "amount": typ.amount,
                    "repeat_unit": typ.repeat_unit,
                    "name": p.name,
                    "trial_days": typ.trial_days,
                    "status_after_retry": typ.status_after_retry,
                    "currency": "MXN",
                    "status": typ.active,
                    "openpay_id": typ.openpay_id,
                    "taggerin_id": p.id,
                    "description": p.description
                  }
                list_plan.append(dict_t)
            dict_plan = {"plans": list_plan}
            return Response(dict_plan, status=status.HTTP_200_OK)
            #creditCard = openpay.createCreditCard(request.data)
        except ValueError as e:
            return Response({'error': str(e)}, status=status.HTTP_406_NOT_ACCEPTABLE)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_406_NOT_ACCEPTABLE)
        #pprint (vars(customer))
        #print(creditCard)


    def create(self, request):
        if request.user and request.auth:
            user = self.get_object(request.user.pk)
        else:
            return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)
        openpay = PaymentAdapter()
        bd_plan = TGPlan()
        try:
            plan = openpay.createPlan(request.data)
            openpay_id = plan['id']
            bd_plan.savePlan(plan, openpay_id)

            #creditCard = openpay.createCreditCard(request.data)
        except ValueError as e:
            return Response({'error': str(e)}, status=status.HTTP_406_NOT_ACCEPTABLE)
        except AttributeError as e:
            return Response({'error': str(e)}, status=status.HTTP_406_NOT_ACCEPTABLE)
        except Exception as e:
            return Response({'error':str(e)}, status=status.HTTP_406_NOT_ACCEPTABLE)
        #pprint (vars(customer))
        #print(creditCard)

        return Response(plan, status=status.HTTP_200_OK)