import arrow
from django.core.exceptions import FieldDoesNotExist, FieldError, ValidationError
from django.db import transaction, IntegrityError
from django.db.transaction import TransactionManagementError
from payments.models import Payments, Subscription
from plans.models import Plan, Type
from datetime import date
import datetime

__author__ = 'isabido'

class TGPlan(object):
    def __init__(self):
        self.plan = None
        self.type = None

    def savePlan(self, plan, openpay_id):
         with transaction.atomic():
            try:
                self.plan = Plan.objects.create(**self.getPlanDict(plan))
                self.type = Type.objects.create(**self.getTypeDict(plan, True, openpay_id, self.plan.id))
                return True
            except ValueError as e:
                raise Exception(e)
            except IntegrityError as e:
                raise Exception(e)
            except FieldDoesNotExist as e:
                raise Exception(e)
            except FieldError as e:
                raise Exception(e)
            except ValidationError as e:
                raise Exception(e)
            except TransactionManagementError as e:
                raise TransactionManagementError(e)
            except TypeError as e:
                raise TypeError(e)
            except AttributeError as e:
                raise AttributeError(e)

    def getPlanDict(self, plan):
        print("En dict")
        print(plan)
        plan_dict = {
            "description": plan['description'], "name": plan['name']
        }
        print(plan_dict)
        return plan_dict

    def getTypeDict(self, plan, active, openpay_id, tg_plan):
        type_dict = {
            "active": active, "status_after_retry": plan['status_after_retry'], "repeat_unit": plan['repeat_unit'],
            "trial_days": plan['trial_days'], "repeat_every": plan['repeat_every'],
            "amount": plan['amount'], "openpay_id": openpay_id, "plan_id": tg_plan
        }
        return type_dict

    def verifyPlan(self):
        pass

    def verifyEndPlan(self, id):
        try:
            payment = Subscription.objects.get(user_id = id)
            days = date.today() - payment.charge_date
            if days.days >= 30:
                self.removeActualPlan(payment)
                return True
            else:
                return False
        except Exception as e:
            return False

    def removeActualPlan(self, payment):
         plan = Plan.objects.get(name="PLAN LIGHT")
         payment.op_subscription = "Taggerin"
         payment.creation_date = arrow.now().isoformat()
         payment.status = 'Active'
         payment.period_end_date = datetime.date.today()+datetime.timedelta(days=5000)
         payment.charge_date = datetime.date.today()
         payment.trial_end_date = datetime.date.today()
         payment.op_plan_id = "Taggerin"
         payment.current_period_number = 0
         payment.plan_id = plan.id
         payment.save()


