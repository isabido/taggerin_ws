
from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Plan(models.Model):
    description = models.CharField(max_length=500, null=False)
    name = models.CharField(max_length=100, null=False)


class Type(models.Model):
    plan = models.ForeignKey(Plan, related_name='type_plan', null=False)
    active = models.BooleanField(null=False)
    status_after_retry = models.CharField(max_length=50)
    trial_days = models.IntegerField()
    repeat_every = models.IntegerField()
    repeat_unit = models.CharField(max_length=25)
    openpay_id = models.CharField(max_length=100)
    amount = models.DecimalField(max_digits=10, decimal_places=2)



