# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Plan',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('description', models.CharField(max_length=500)),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Type',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('active', models.BooleanField()),
                ('status_after_retry', models.CharField(max_length=50)),
                ('trial_days', models.IntegerField()),
                ('repeat_every', models.IntegerField()),
                ('repeat_unit', models.CharField(max_length=25)),
                ('openpay_id', models.CharField(max_length=100)),
                ('amount', models.DecimalField(max_digits=10, decimal_places=2)),
                ('plan', models.ForeignKey(related_name='type_plan', to='plans.Plan')),
            ],
        ),
    ]
