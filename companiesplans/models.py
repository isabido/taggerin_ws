from django.db import models
from django.contrib.auth.models import User
from django.contrib.gis.db import models
# Create your models here.
from plans.models import Plan


class CompanyPlan(models.Model):
    user = models.OneToOneField(User, primary_key=True)
    plan = models.OneToOneField(Plan)
    duration = models.IntegerField()
    updatedAt = models.DateTimeField(auto_now=True)
    finishAt = models.DateTimeField()

