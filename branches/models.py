from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Branches(models.Model):
    user = models.ForeignKey(User)
    name = models.CharField(max_length=100)
    