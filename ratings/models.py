from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class Rating(models.Model):
    user = models.ForeignKey(User, related_name="raitng_user")
    createdAt = models.DateTimeField(auto_now_add=True)
    company = models.ForeignKey(User, related_name="company_rating_user")
    price = models.IntegerField(null=True, default=0)
    service = models.IntegerField(null=True, default=0)
    quality = models.IntegerField(null=True, default=0)
    location = models.IntegerField(null=True, default=0)