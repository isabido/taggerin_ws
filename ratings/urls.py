__author__ = 'isabido'
from .views import RatingViewSet

RatingView = RatingViewSet.as_view({'post': 'create', 'put': 'update', 'get':'retrieve'})