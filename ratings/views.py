from django.shortcuts import render

# Create your views here.
from django.shortcuts import render

# Create your views here.
from pprint import pprint
from django.contrib.auth.models import User
from django.db.models import Q, Avg, Min, Max
from rest_framework import status
from django.http import Http404
from django.shortcuts import render

# Create your views here.
from oauth2_provider.ext.rest_framework import OAuth2Authentication
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.exceptions import ValidationError
from Company.models import Location
from rest_framework import permissions
from checkins.models import CheckIn
from plans.models import Plan
from ratings.models import Rating
from search.serializers import SearchSerializer, GetProfileSerializer
from users.models import Profile
from users.permissions import UserInSessionPermission, UserTypeCompany, UserTypeNormal, BothTypeCompany
from django.contrib.gis.geos import *
from django.contrib.gis.measure import D


class RatingViewSet(viewsets.ModelViewSet):

    def get_object(self, pk):
        try:
            obj = User.objects.get(pk=pk)
            #self.check_object_permissions(self.request, obj)
            return obj
        except User.DoesNotExist:
            raise Http404
    authentication_classes = (OAuth2Authentication,)
    #permission_classes = (permissions.IsAuthenticated, UserInSessionPermission, UserTypeCompany,)

    def get_permissions(self):
        return (permissions.IsAuthenticated(), UserInSessionPermission(), BothTypeCompany(),)



    def create(self, request):
        try:
            if request.user and request.auth:
                user = User.objects.get(pk=request.user.pk)
            else:
                return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)

            service = int(request.data['service'])
            company = int(request.data['company'])
            price = int(request.data['price'])
            quality = int(request.data['quality'])
            location = int(request.data['location'])
            values = [1,2,3,4,5]
            company_band = User.objects.get(pk=company)
            if company_band and company != user.id:
                if service not in values or price not in values or quality not in values or location not in values:
                    return Response({'error': 'La calificación debe estar en el rango de 1 a 5'}, status=status.HTTP_412_PRECONDITION_FAILED)
                updated_values = {'service': service, 'price':price, 'quality':quality, 'location': location}
                obj, created = Rating.objects.update_or_create(user_id=user.id, company_id=company, defaults=updated_values)
                return Response({}, status=status.HTTP_204_NO_CONTENT)
            else:
                return Response({'error': 'La empresa no existe'}, status=status.HTTP_412_PRECONDITION_FAILED)


        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)

    def update(self, request):
        try:
            if request.user and request.auth:
                user = User.objects.get(pk=request.user.pk)
            else:
                return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)

            service = int(request.data['service'])
            company = int(request.data['company'])
            price = int(request.data['price'])
            quality = int(request.data['quality'])
            location = int(request.data['location'])
            values = [1,2,3,4,5]
            company_band = User.objects.get(pk=company)
            if company_band and company != user.id:
                if service not in values or price not in values or quality not in values or location not in values:
                    return Response({'error': 'La calificación debe estar en el rango de 1 a 5'}, status=status.HTTP_412_PRECONDITION_FAILED)
                updated_values = {'service': service, 'price':price, 'quality':quality, 'location': location}
                obj, created = Rating.objects.update_or_create(user_id=user.id, company_id=company, defaults=updated_values)
                return Response({}, status=status.HTTP_204_NO_CONTENT)
            else:
                return Response({'error': 'La empresa no existe'}, status=status.HTTP_412_PRECONDITION_FAILED)


        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)

    def retrieve(self, request, pk=None):
        try:
            if request.user and request.auth:
                user = User.objects.get(pk=request.user.pk)
            else:
                return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)
            if pk == None:
                return Response({'error': "Debe enviar el identificador de la empresa"}, status=status.HTTP_400_BAD_REQUEST)
            price = Rating.objects.filter(company_id=pk).aggregate(avg=Avg('price'), min=Min('price'), max=Max('price'))
            service = Rating.objects.filter(company_id=pk).aggregate(avg=Avg('service'), min=Min('service'), max=Max('service'))
            quality = Rating.objects.filter(company_id=pk).aggregate(avg=Avg('quality'), min=Min('quality'), max=Max('quality'))
            location = Rating.objects.filter(company_id=pk).aggregate(avg=Avg('location'), min=Min('location'), max=Max('location'))
            resp = {
                'service': service,
                'quality': quality,
                'price': price,
                'location': location
            }
            return Response(resp, status=status.HTTP_200_OK)
        except ValidationError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print("HHH")
            return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)