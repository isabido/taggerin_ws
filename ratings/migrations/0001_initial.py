# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Rating',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('createdAt', models.DateTimeField(auto_now_add=True)),
                ('price', models.IntegerField(default=0, null=True)),
                ('service', models.IntegerField(default=0, null=True)),
                ('quality', models.IntegerField(default=0, null=True)),
                ('location', models.IntegerField(default=0, null=True)),
                ('company', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='company_rating_user')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='raitng_user')),
            ],
        ),
    ]
