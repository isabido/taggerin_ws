from pprint import pprint
from django.db.models import Q
from conversations.models import Conversation

__author__ = 'isabido'
from rest_framework import serializers
from django.contrib.auth.models import User
from users.models import Profile
from Company.models import Notification
from django.db import transaction, IntegrityError, Error


class NotificationSerializer(serializers.Serializer):
    #notifications = PruebadSerializer(many=True)
    id = serializers.IntegerField()
    user_id = serializers.IntegerField()
    notification_type = serializers.IntegerField()
    description = serializers.CharField(max_length=500)
    createdAt = serializers.DateTimeField()
    match = serializers.IntegerField()
    conversation_id = serializers.SerializerMethodField('getConversationId')

    def getConversationId(self,data):
        print('get')
        try:
            conversation_id = Conversation.objects.get(Q(sender_id = data.user_id) | Q(receiver_id=data.user_id), Q(sender_id = data.match) | Q(receiver_id=data.match))
            if conversation_id:
                return conversation_id.id
            else:
                return None
        except Conversation.DoesNotExist:
            conversation_id = None
        except Exception:
            conversation_id = None

