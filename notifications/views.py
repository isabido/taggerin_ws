from django.shortcuts import render

# Create your views here.
from django.utils.datastructures import MultiValueDictKeyError
from Company.models import Notification
from users.models import Profile
from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.response import Response
from users.permissions import UserInSessionPermission, UserTypeCompany
from oauth2_provider.ext.rest_framework import OAuth2Authentication
from rest_framework import permissions
from django.http import Http404
from django.contrib.auth.models import User
from rest_framework import status
from django.db import transaction, IntegrityError, Error
from .serializers import NotificationSerializer

class NotificationViewSet(viewsets.ViewSet):
    def get_object(self, pk):
        try:
            obj = User.objects.get(pk=pk)
            #self.check_object_permissions(self.request, obj)
            return obj
        except User.DoesNotExist:
            raise Http404
    authentication_classes = (OAuth2Authentication,)

    def get_permissions(self):
        if self.request.method == 'POST':
            return (permissions.AllowAny(),)

        return (permissions.IsAuthenticated(), UserInSessionPermission(), UserTypeCompany(),)

    def retrieve(self, request, pk=None):
        if request.user and request.auth:
            user = self.get_object(request.user.pk)
        else:
            return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)

        # user = User.objects.filter(pk=request.user.pk).select_related()
        try:
            offset = (0, int(request.query_params['offset'])) [request.query_params['offset'] is not None]
            limit = (20, int(request.query_params['limit'])) [request.query_params['limit'] is not None]
            notifications = Notification.objects.filter(user=user)[offset:limit]
        except MultiValueDictKeyError:
            notifications = Notification.objects.filter(user=user)
        #print(notifications)
        serializer = NotificationSerializer(notifications, many=True)
        notify = {"notifications": serializer.data}
        return Response(notify, status=status.HTTP_200_OK)