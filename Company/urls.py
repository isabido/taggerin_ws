__author__ = 'isabido'
from .views import CompanyViewSet, TagsViewSet, LocationViewSet, UploadImageViewSet, ProfilePageViewSet, CategoryViewSet, PublicProfilePageViewSet

CompanyView = CompanyViewSet.as_view({'post': 'create', 'put': 'update', 'delete':'destroy', 'get':'retrieve'})

TagsView = TagsViewSet.as_view({'post':'create', 'get': 'retrieve', 'delete':'destroy'})

LocationView = LocationViewSet.as_view({'post':'create', 'put': 'update', 'get': 'retrieve'})

UploadProfileImageCompany = UploadImageViewSet.as_view({'put':'update', 'post':'create'})
ProfilePageCompany = ProfilePageViewSet.as_view({'post':'create', 'get': 'retrieve','put': 'update'})
CategoryPageCompany = CategoryViewSet.as_view({'get': 'retrieve'})

PublicProfilePageCompany = PublicProfilePageViewSet.as_view({'get': 'retrieve'})