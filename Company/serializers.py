from pprint import pprint
from django.contrib.gis.geos import Point
from django.db.models import Q
from rest_framework.exceptions import ValidationError
from rest_framework_gis.serializers import GeoFeatureModelSerializer
from payments.models import Subscription
from plans.models import Plan
from users.TaggerinPermissions.TGPermissions import TgPermissions

__author__ = 'isabido'
from rest_framework import serializers
from django.contrib.auth.models import User
from users.models import Profile
from .models import TagOffered, Location, ProfilePage, PageImages, Notification, Category, Company_Tags_Offered, \
    Company_Tags_Requiere
from django.db import transaction, IntegrityError, Error
from activations import utils
from django.conf import settings
from boto.s3.connection import S3Connection
from boto.s3.key import Key
from activations.utils import upload_to_s3

class TagOfferedSerializer(serializers.Serializer):
    name = serializers.CharField()
    id = serializers.IntegerField(required=False)

    def validate_name(self, data):
        print("here")
        pprint(data)
        return data


class GetCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'category')

class Company(serializers.ModelSerializer):
    profile_image = serializers.URLField(required=False, allow_null=True)
    category = serializers.IntegerField(required=False, allow_null=True)
    password = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    rfc = serializers.CharField(max_length=13, min_length=10,allow_null=True, required=False, allow_blank=True)
    email = serializers.EmailField(required=False)

    #tagoffered = TagOfferedSerializer(many=True)
    #req_tag = RequestedTagSerializer(many=True)

    class Meta:
        model = User
        fields = ('id','email', 'first_name','password','profile_image', 'category', 'rfc',)
        depth = 2

    def create(self, validated_data):
        from pprint import pprint
        # print(validated_data)
        #_post = validated_data.pop('profile')
        rfc = validated_data.pop('rfc')
        p_image = validated_data.pop('profile_image')
        cat = validated_data.pop('category')
        if validated_data.get('email') != None and validated_data.get('password'):
            #_tags_offered = validated_data.pop('tagoffered')
            with transaction.atomic():
                user = None
                try:
                    user = User.objects.create_user(username=validated_data.get('email'),  **validated_data)
                    user.is_active = False
                    user.save()
                    print(cat)
                except Error as e:
                    error = dict()
                    error['error'] = e
                    raise serializers.ValidationError(error)
                try:
                    category = Category.objects.get(pk=cat)
                    # print(category)
                    profile = Profile.objects.create(user=user, user_type=0,rfc=rfc, category_id=category.pk, profile_image=p_image)
                    profile.save()
                    utils.create_activation_profile(user)
                    utils.send_activation_email(user)

                    dict_user = {
                    'id': user.id,
                    'email': user.email,
                    "first_name": user.first_name,
                    "profile_image": profile.profile_image,
                    "rfc": profile.rfc,
                    "category":profile.category.pk,
                    "password": user.password
                        }
                    return dict_user
                except Error as e:
                    error = dict()
                    error['error'] = e
                    raise serializers.ValidationError(error)

                return user
        else:
            raise serializers.ValidationError("Email and password are necessary")
            # for tag in _tags_offered:
            #     tagoffered = TagOffered.objects.create(name=tag["name"])
            #     tagoffered.user.add(user)
            #
            # user.tagoffered = _tags_offered


    def update(self, instance, validated_data):
        #_post = validated_data.pop('profile')
        # _tags_offered = validated_data.pop('tagoffered')
        print("update")
        with transaction.atomic():
            #instance.username = validated_data.get('email', instance.email)
            try:
                #instance.email = validated_data.get('email', instance.email)
                if validated_data.get('password'):
                    instance.set_password(validated_data.get('password', instance.password))
                if validated_data.get('first_name'):
                    instance.first_name = validated_data.get('first_name', instance.first_name)
                #instance.password = validated_data.get('password', instance.password)
                if validated_data.get('category'):
                    category = Category.objects.get(pk=validated_data.get('category'))
                prof = Profile.objects.get(pk=instance.id)
                if validated_data.get('profile_image'):
                    prof.profile_image = validated_data.get('profile_image', prof.profile_image)
                if validated_data.get('rfc'):
                    prof.rfc = validated_data.get('rfc', prof.rfc)
                if validated_data.get('category'):
                    prof.category_id = validated_data.get('category')
                # u = User.objects.get(pk=instance.pk)
                if prof.user_type != 0:
                    prof.user_type = 0
                #t = instance.profile.tagoffered.all()
                # for tag in _tags_offered:
                #     obj, created = TagOffered.objects.get_or_create(name=tag["name"])
                #     prof.tagoffered.add(obj)

                instance.save()
                prof.save()
                dict_user = {
                'id': instance.id,
                'email': instance.email,
                "first_name": instance.first_name,
                "profile_image": prof.profile_image,
                "rfc": prof.rfc,
                "category":prof.category_id,
                "password": instance.password
                    }
            except Error as e:
                error = dict()
                error['error'] = e
                raise serializers.ValidationError(error)
        # for tag in _tags_offered:
        #     TagOffered.objects.filter(user=instance.id).update_or_create(name=tag["name"])
        #
        #
        # instance.tagoffered = _tags_offered
        return dict_user

class CompanyProfile(serializers.ModelSerializer):
    category = GetCategorySerializer()
    class Meta:
        model = Profile
        fields = ('profile_image', 'category', 'rfc',)

class RequestedTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = TagOffered
        fields = ('id', 'name',)

class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        # geo_field = "point"
        # id_field = False
        fields = ('city', 'state', 'country', 'address','point',)

    def create(self, validated_data):
        #_loc = validated_data.pop('point')

        with transaction.atomic():
            #user = User.objects.get(id=)
            try:
                location = Location.objects.create(**validated_data)
            except Error as e: # catch *all* exceptions
                error = dict()
                error['error'] = e
                raise serializers.ValidationError(error)

        return location

    def update(self, instance, validated_data):
        # _tags_offered = validated_data.pop('tagoffered')
        with transaction.atomic():
            try:
                instance.point = validated_data.get('point', instance.point)
                instance.city = validated_data.get('city', instance.city)
                instance.address = validated_data.get('address', instance.address)
                instance.country = validated_data.get('country', instance.country)
                instance.state = validated_data.get('state', instance.state)
                instance.save()
            except Error as e: # catch *all* exceptions
                error = dict()
                error['error'] = e
                raise serializers.ValidationError(error)
        return instance


class GetCompanySerializer(serializers.Serializer):
    def getTagoffered(self,data):
        ux = []
        try:
            print("get")
            user = Company_Tags_Offered.objects.filter(profile_id=data.id, active=True)
            #pprint(user)
            print("after user")
            for u in user:
                model= {
                    'id': u.tag_id.id,
                    'name':u.tag_id.name
                }
                ux.append(model)
                pprint(u.tag_id.id)
            return ux
        except Exception as e:
            return ux

    def getRequiretag(self,data):
            print("get")
            ux = []
            try:
                user = Company_Tags_Requiere.objects.filter(profile_id=data.id, active=True)
                #pprint(user)
                print("after user require")
                ux = []
                for u in user:
                    model= {
                        'id': u.tag_id.id,
                        'name':u.tag_id.name
                    }
                    ux.append(model)
                    pprint(u.tag_id.id)
                return ux
            except Exception as e:
                return ux

    profile_image = serializers.URLField(source='user_profile.profile_image')
    category = GetCategorySerializer(source='user_profile.category')
    rfc = serializers.CharField(source='user_profile.rfc', max_length=13)
    location = LocationSerializer(source='user_location', read_only=True)
    id = serializers.IntegerField()
    first_name = serializers.CharField()
    tagoffered = serializers.SerializerMethodField('getTagoffered')
    requiredtag = serializers.SerializerMethodField('getRequiretag')
    email = serializers.EmailField()
    username = serializers.EmailField()




class TaggsCompanySerializer(serializers.Serializer):
    tagoffered = TagOfferedSerializer(many=True, source='user_tagoffered')
    requiredtag = RequestedTagSerializer(many=True, source='user_requiretag')

    def validate_tagoffered(self, value):
        """
        Check that the blog post is about Django.
        """
        print("tag")
        if len(value)>6:
            raise serializers.ValidationError("Only 6 tags accepted")
        return value

    def validate_requiredtag(self, value):
        """
        Check that the blog post is about Django.
        """
        if len(value)>6:
            raise serializers.ValidationError("Only 6 tags accepted")
        return value


    def create(self, validated_data):
        pprint(validated_data)
        _tagOffered = validated_data.pop('user_tagoffered')
        _requiredTags = validated_data.pop('user_requiretag')
        #_tags_offered = validated_data.pop('tagoffered')
        with transaction.atomic():
            try:
                profile = User.objects.get(id=validated_data['owner'].pk)
                profile.user_tagoffered.clear()
                profile.user_requiretag.clear()
                list_requireTags = list()
                list_offeredTags = list()
                for offered in _tagOffered:
                    list_offeredTags.append(offered['name'].lower())
                    obj, created = TagOffered.objects.get_or_create(name=offered["name"].lower())
                    #obj.tagoffered.active = True
                    #pprint()
                    #profile.user_tagoffered.add(obj)
                    relational_tag = Company_Tags_Offered(profile_id=profile, tag_id=obj, active=True)
                    relational_tag.save()
                for required in _requiredTags:
                    list_requireTags.append(required['name'].lower())
                    req, created = TagOffered.objects.get_or_create(name=required['name'].lower())
                    #req.requiredtag.active = True
                    #profile.user_requiretag.add(req)
                    relational_tag = Company_Tags_Requiere(profile_id=profile, tag_id=req, active=True)
                    relational_tag.save()
                users = utils.search_tags(list_requireTags)

                # if not users:
                #     raise serializers.ValidationError(users)
                plan = Plan.objects.get(name="PLAN LIGHT")
                if users:
                    for user in users:
                        if(user.id !=validated_data['owner'].id):
                            print("before subscr")
                            try:
                                subscription = Subscription.objects.filter(user_id=user.id).filter(~Q(plan_id=plan.id))
                            except Exception as e:
                                continue
                            print("subscr")
                            company = validated_data['owner'].first_name
                            company_match = "La empresa "+company+ " ofrece los siguientes servicios que podrían interesarle: "
                            description=""
                            for tag in _requiredTags:
                                description = description + tag['name'] + ", "
                            description = company_match + description
                            Notification.objects.create(user_id=user.id, notification_type=1, description=description[:-2], match=validated_data['owner'].id)
                users = None
                users = utils.search_tagsRequired(list_offeredTags)

                if users:
                    for user in users:
                        if(user.id !=validated_data['owner'].id):
                            try:
                                subscription = Subscription.objects.filter(user_id=user.id).filter(~Q(plan_id=plan.id))
                            except Exception as e:
                                continue
                            print("subscr")
                            company = validated_data['owner'].first_name
                            company_match = "La empresa "+company+ " requiere los siguientes servicios: "
                            description=""
                            for tag in _requiredTags:
                                description = description + tag['name'] + ", "
                            description = company_match + description
                            Notification.objects.create(user_id=user.id, notification_type=1, description=description[:-2], match=validated_data['owner'].id)

                return profile
            except Error as e: # catch *all* exceptions
                error = dict()
                error['error'] = str(e)
                raise serializers.ValidationError(error)
            except Exception as e:
                raise Exception(e)



class CompanyLocationSerializer(serializers.Serializer):
    point = LocationSerializer()

    def create(self, validated_data):
        _loc = validated_data.pop('point')
        point = Point(_loc['coordinates'][0], _loc['coordinates'][1])
        with transaction.atomic():
            user = User.objects.get(id=validated_data['owner']['id'])
            location = Location.objects.create(point=point, user=user,**validated_data)

        return location

class ImageSerializer(serializers.Serializer):
    image = serializers.FileField(allow_empty_file=True)

    def create(self, validated_data):
        file = validated_data['image']
        filename = file.name
        content = file.content
        upload_to_s3(filename, content)

        with transaction.atomic():
            try:
                profile = Profile.objects.get(user=validated_data['user'])
            except Error as e: # catch *all* exceptions
                error = dict()
                error['error'] = e
                raise serializers.ValidationError(error)

        return profile

    def update(self, instance, validated_data):
        # _tags_offered = validated_data.pop('tagoffered')

        file = validated_data['image']
        url = upload_to_s3(file)
        # try:
        #     instance.profile_image = url
        #     instance.save()
        # except Error as e: # catch *all* exceptions
        #     error = dict()
        #     error['error'] = e
        #     raise serializers.ValidationError(error)
        return url


class PageImagesSerializer(serializers.ModelSerializer):
    #profile_page = ProfilePageSerializer(many=True)
    class Meta:
        model = PageImages
        fields = ('url', 'image_description',)

class ProfilePageSerializer(serializers.ModelSerializer):
    profile_images = PageImagesSerializer(many=True)
    class Meta:
        model = ProfilePage
        fields = ('description', 'web', 'profile_images',)

    def create(self, validated_data):
        _img_pages = validated_data.pop('profile_images')
        with transaction.atomic():
            try:
                page = ProfilePage.objects.create(**validated_data)
                for img in _img_pages:
                    PageImages.objects.create(p_page= page,**img)
            except Error as e:
                error = dict()
                error['error'] = e
                raise serializers.ValidationError(error)
        return page

    def update(self, instance, validated_data):
        _img_pages = validated_data.pop('profile_images')
        with transaction.atomic():
            try:
                instance.delete()
                PageImages.objects.filter(p_page_id=validated_data['user'].pk).delete()
                print("SI")
                page = ProfilePage.objects.create(**validated_data)
                for img in _img_pages:
                    PageImages.objects.create(p_page= page,**img)
            except Error as e:
                error = dict()
                error['error'] = e
                raise serializers.ValidationError(error)
        return page

class GetProfileSerializer(serializers.ModelSerializer):
    profile_image = serializers.URLField(source='user_profile.profile_image')
    category = serializers.CharField(source='user_profile.category.category',max_length=50)
    rfc = serializers.CharField(source='user_profile.rfc', max_length=13)
    location = LocationSerializer(source='user_location', read_only=True)
    tagoffered = serializers.SerializerMethodField('getTagoffered')
    requiredtag = serializers.SerializerMethodField('getRequiretag')
    profile_page = ProfilePageSerializer()

    def getTagoffered(self, obj):
        list_tags = []
        try:
            p = TgPermissions()
            if p.isFreePlan(obj):
                offered = Company_Tags_Offered.objects.filter(Q(profile_id = obj.id), Q(active = True))[:3]
            else:
                offered = Company_Tags_Offered.objects.filter(Q(profile_id = obj.id), Q(active = True))

            for tag in offered:
                model = {
                    "id": tag.tag_id.id,
                    "name": tag.tag_id.name
                }
                list_tags.append(model)

            return list_tags
        except Company_Tags_Requiere.DoesNotExist as e:
            return list_tags
        except ValidationError as e:
            raise  ValidationError(e)
        except ValueError as e:
            raise ValueError(e)
        except Exception as e:
            raise Exception(e)

    def getRequiretag(self, obj):
        list_tags = []
        try:
            p = TgPermissions()
            if p.isFreePlan(obj):
                offered = Company_Tags_Requiere.objects.filter(Q(profile_id = obj.id), Q(active = True))[:3]
            else:
                offered = Company_Tags_Requiere.objects.filter(Q(profile_id = obj.id), Q(active = True))
            for tag in offered:
                model = {
                    "id": tag.tag_id.id,
                    "name": tag.tag_id.name
                }
                list_tags.append(model)
            return list_tags
        except Company_Tags_Requiere.DoesNotExist as e:
            return list_tags
        except ValidationError as e:
            raise  ValidationError(e)
        except ValueError as e:
            raise ValueError(e)
        except Exception as e:
            raise Exception(e)

    class Meta:
        model = User
        fields = ('id','first_name', 'email', 'username', 'password', 'location', 'profile_image', 'category', 'rfc', 'tagoffered', 'requiredtag', 'profile_page')

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category

class GetTagsSerializer(serializers.Serializer):
    tagoffered = serializers.SerializerMethodField('getTagoffered')
    requiredtag = serializers.SerializerMethodField('getTagrequired')

    def getTagoffered(self, obj):
        list_tags = []
        try:
            p = TgPermissions()
            if p.isFreePlan(obj):
                offered = Company_Tags_Offered.objects.filter(Q(profile_id = obj.id), Q(active = True))[:3]
            else:
                offered = Company_Tags_Offered.objects.filter(Q(profile_id = obj.id), Q(active = True))

            for tag in offered:
                model = {
                    "id": tag.tag_id.id,
                    "name": tag.tag_id.name
                }
                list_tags.append(model)

            return list_tags
        except Company_Tags_Requiere.DoesNotExist as e:
            return list_tags
        except ValidationError as e:
            raise  ValidationError(e)
        except ValueError as e:
            raise ValueError(e)
        except Exception as e:
            raise Exception(e)

    def getTagrequired(self, obj):
        list_tags = []
        try:
            p = TgPermissions()
            if p.isFreePlan(obj):
                offered = Company_Tags_Requiere.objects.filter(Q(profile_id = obj.id), Q(active = True))[:3]
            else:
                offered = Company_Tags_Requiere.objects.filter(Q(profile_id = obj.id), Q(active = True))
            for tag in offered:
                model = {
                    "id": tag.tag_id.id,
                    "name": tag.tag_id.name
                }
                list_tags.append(model)
            return list_tags
        except Company_Tags_Requiere.DoesNotExist as e:
            return list_tags
        except ValidationError as e:
            raise  ValidationError(e)
        except ValueError as e:
            raise ValueError(e)
        except Exception as e:
            raise Exception(e)




