from pprint import pprint
from boto.s3.connection import S3Connection
from boto.s3.key import Key
from django.conf import settings
from rest_framework.exceptions import ValidationError
from rest_framework_gis import serializers
from activations.utils import upload_to_s3
from plans.Plan import TGPlan
from users.TaggerinPermissions.TGPermissions import TgPermissions
from users.models import Profile
from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.response import Response
from users.permissions import UserInSessionPermission, UserTypeCompany, isFreePlan
from rest_framework.permissions import AllowAny
from rest_framework import permissions
from django.http import Http404
from django.contrib.auth.models import User
from rest_framework import status
from .serializers import Company, ImageSerializer, TagOfferedSerializer, LocationSerializer, GetCompanySerializer, RequestedTagSerializer, TaggsCompanySerializer, \
    CompanyLocationSerializer, ProfilePageSerializer, PageImagesSerializer, GetProfileSerializer, CategorySerializer, \
    GetTagsSerializer
from oauth2_provider.ext.rest_framework import OAuth2Authentication
from .models import TagOffered, Location, ProfilePage, Category
from rest_framework.parsers import FileUploadParser, MultiPartParser, FormParser
import magic
from django.db import transaction, IntegrityError, Error


# CRUD Company.
class CompanyViewSet(viewsets.ViewSet):

    def get_object(self, pk):
        try:
            obj = User.objects.get(pk=pk)
            #self.check_object_permissions(self.request, obj)
            return obj
        except User.DoesNotExist:
            raise Http404
    authentication_classes = (OAuth2Authentication,)
    #permission_classes = (permissions.IsAuthenticated, UserInSessionPermission, UserTypeCompany,)

    def get_permissions(self):
        if self.request.method == 'POST':
            return (permissions.AllowAny(),)

        return (permissions.IsAuthenticated(), UserInSessionPermission(), UserTypeCompany(), )

    def retrieve(self, request, pk=None):
        try:
            if request.user and request.auth:
                user = self.get_object(request.user.pk)
            else:
                return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)

            tg_p = TgPermissions()
            tg_p.isSubscriptionValid(user)
            # user = User.objects.filter(pk=request.user.pk).select_related()
            #user2 = User.objects.filter(id=user.id).exclude(user_tagoffered=True)

            #pprint(user2.user_tagoffered)
            serializer = GetCompanySerializer(user)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except ValidationError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)

    def create(self, request):
        # permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
        # Both UserProfileSerializer and UserProfileSerializer are required
        # in order to validate and save data on their associated models.
        try:
            if request.auth:
                error = {"error":"You don't have permission"}
                return Response(error,status=status.HTTP_401_UNAUTHORIZED)

            user_serializer = Company(data=request.data)
            if user_serializer.is_valid():

                user_serializer.save()
                return Response(user_serializer.data, status=status.HTTP_201_CREATED)
            # Combine errors from both serializers.

            return Response(user_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except ValidationError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)

    def update(self, request, pk=None):
        try:
            if request.user and request.auth:
                user = self.get_object(request.user.pk)
            else:
                return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)
            user = self.get_object(request.user.pk)
            print("HOLA")
            user_serializer = Company(user, data=request.data)
            if user_serializer.is_valid():
                user_serializer.save()
                #user_serializer.save(#owner=self.request.user)
                return Response(user_serializer.data, status=status.HTTP_200_OK)
            errors = dict()
            errors.update(user_serializer.errors)
            return Response(errors, status=status.HTTP_400_BAD_REQUEST)
        except ValidationError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)


    def destroy(self, request, pk=None):
        if request.user and request.auth:
            user = self.get_object(request.user.pk)
        else:
            return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)
        user.delete()
        message = {"message":"Deleted Company"}
        return Response(message, status=status.HTTP_200_OK)



class TagsOfferedViewSet(viewsets.ModelViewSet):
    model = TagOffered
    serializer_class = TagOfferedSerializer



#Crea y obtiene los tags de la empresa
class TagsViewSet(viewsets.ModelViewSet):

    def get_object(self, pk):
        try:
            obj = User.objects.get(pk=pk)

            return obj
        except User.DoesNotExist:
            raise Http404
    authentication_classes = (OAuth2Authentication,)
    permission_classes = (permissions.IsAuthenticated, UserInSessionPermission, UserTypeCompany,)

    # def get_permissions(self):
    #     return (permissions.IsAuthenticated(), UserInSessionPermission(), UserTypeCompany(),)

    def retrieve(self, request, pk=None):
        if request.user and request.auth:
            user = User.objects.get(pk=request.user.pk)
        else:
            return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)
        # user = User.objects.filter(pk=request.user.pk).select_related()
        try:

            tg_p = TgPermissions()
            tg_p.isSubscriptionValid(user)
            tags = GetTagsSerializer(user)
            mess = {"mes":"hola"}
            return Response(tags.data, status=status.HTTP_200_OK)
        except ValidationError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)

    def create(self, request):
        if request.auth:
            pass
        else:
            error = {"error":"You don't have permission"}
            return Response(error,status=status.HTTP_401_UNAUTHORIZED)
        try:
            if len(request.data['tagoffered']) > 6:
                error =  {"error": "Only 6 tags accepted - tagoffered"}
                return Response(error,status=status.HTTP_400_BAD_REQUEST)
            elif len(request.data['requiredtag']) > 6:
                error =  {"error": "Only 6 tags accepted - requiredtag"}
                return Response(error,status=status.HTTP_400_BAD_REQUEST)
            tg_permission = TgPermissions()
            if tg_permission.isFreePlan(request.user):
                if len(request.data['tagoffered']) > 3:
                    error =  {"error": "Only 3 tags accepted for Plan Light - tagoffered"}
                    return Response(error,status=status.HTTP_400_BAD_REQUEST)

            tags = TaggsCompanySerializer(data=request.data)
            #requested_tags = RequestedTagSerializer(data=request.data)
            if tags.is_valid():
                tags.save(owner=request.user)
                return Response(tags.data, status=status.HTTP_201_CREATED)
            # Combine errors from both serializers.
            return Response(tags.errors, status=status.HTTP_400_BAD_REQUEST)
        except ValidationError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)


    def destroy(self, request, pk=None):
        try:
            if request.user and request.auth:
                profile = Profile.objects.get(pk=request.user.pk)
            else:
                return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)
            profile.tagoffered.clear()
            profile.requiredtag.clear()
            message = {"message":"Both lists tags were deleted."}
            return Response(message, status=status.HTTP_200_OK)
        except ValidationError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)

class LocationViewSet(viewsets.ModelViewSet):

    def get_object(self, pk):
        try:
            obj = User.objects.get(pk=pk)
            #self.check_object_permissions(self.request, obj)
            return obj
        except User.DoesNotExist:
            raise Http404
    authentication_classes = (OAuth2Authentication,)
    #permission_classes = (permissions.IsAuthenticated, UserInSessionPermission, UserTypeCompany,)

    def get_permissions(self):
        return (permissions.IsAuthenticated(), UserInSessionPermission(), UserTypeCompany(),)

    def retrieve(self, request, pk=None):
        try:
            if request.user and request.auth:
                location = Location.objects.get(pk=request.user.pk)
            else:
                return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)
            # user = User.objects.filter(pk=request.user.pk).select_related()
            _loc = LocationSerializer(location)
            mess = {"mes":"hola"}
            return Response(_loc.data, status=status.HTTP_200_OK)
        except ValidationError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)

    def create(self, request):
        try:
            # permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
            # Both UserProfileSerializer and UserProfileSerializer are required
            # in order to validate and save data on their associated models.
            if request.auth:
                pass
            else:
                error = {"error":"You don't have permission"}
                return Response(error,status=status.HTTP_401_UNAUTHORIZED)

            location = LocationSerializer(data=request.data)
            #requested_tags = RequestedTagSerializer(data=request.data)
            if location.is_valid():
                location.save(user=self.get_object(request.user.pk))
                return Response(location.data, status=status.HTTP_201_CREATED)
            # Combine errors from both serializers.

            return Response(location.errors, status=status.HTTP_400_BAD_REQUEST)
        except ValidationError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)

    def update(self, request, pk=None):
        try:
            if request.user and request.auth:
                user = self.get_object(request.user.pk)
            else:
                return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)
            loc = Location.objects.get(pk=request.user.pk)
            location_serializer = LocationSerializer(loc, data=request.data)
            if location_serializer.is_valid():
                location_serializer.save()
                #user_serializer.save(#owner=self.request.user)
                return Response(location_serializer.data, status=status.HTTP_200_OK)
            errors = dict()
            errors.update(location_serializer.errors)
            return Response(errors, status=status.HTTP_400_BAD_REQUEST)
        except ValidationError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)

class UploadImageViewSet(viewsets.ModelViewSet):
    #parser_classes = (MultiPartParser,)
    parser_classes = (MultiPartParser, FormParser, FileUploadParser,)

    def get_object(self, pk):
        try:
            obj = Profile.objects.get(pk=pk)
            #self.check_object_permissions(self.request, obj)
            return obj
        except User.DoesNotExist:
            raise Http404
    authentication_classes = (OAuth2Authentication,)

    #permission_classes = (permissions.IsAuthenticated, UserInSessionPermission, UserTypeCompany,)

    def get_permissions(self):
        return (permissions.IsAuthenticated(), UserInSessionPermission(), UserTypeCompany(),)

    def update(self, request,format=None):
            try:
                list_formats = ("image/png", "image/jpg", "image/jpeg", "image/gif",)
                try:
                    print("image")

                    file_obj = request.data['image']
                    print(type(file_obj))
                    print("after file_obj")
                    #f = magic.Magic(mime=True, uncompress=True)
                    with magic.Magic(flags=magic.MAGIC_MIME_TYPE) as m:
                        img_buffer = m.id_buffer(file_obj.read())
                        if img_buffer in list_formats:
                            pass
                        else:
                            errors = {"error": "Invalid Type", "type": img_buffer }
                            return Response(errors, status=status.HTTP_406_NOT_ACCEPTABLE)
                    print(str(file_obj.size))
                    if file_obj.size < 2500000:
                       url = True
                    else:
                        errors = {"error": "Image must be less than 2.5MB"}
                        return Response(errors, status=status.HTTP_406_NOT_ACCEPTABLE)
                except ValueError:
                    errors = {"error": str(ValueError)}
                    return Response(errors, status=status.HTTP_406_NOT_ACCEPTABLE)
                except Exception as e:
                    errors = {"error": str(e)}
                    return Response(errors, status=status.HTTP_406_NOT_ACCEPTABLE)
                _image = ImageSerializer(data=request.data)

                if _image.is_valid():
                    try:
                        url = upload_to_s3(file_obj)
                        Profile.objects.filter(user=request.user).update(profile_image=url)
                    except Error as e:
                        error = dict()
                        error['error'] = str(e)
                        return Response(error, status=status.HTTP_409_CONFLICT)
                    except ValueError:
                        errors = {"error": str(ValueError)}
                        return Response(errors, status=status.HTTP_406_NOT_ACCEPTABLE)
                    except Exception as e:
                        errors = {"error": str(e)}
                        return Response(errors, status=status.HTTP_406_NOT_ACCEPTABLE)
                    if url:
                        message = {"url":url}
                        return Response(message,  status=status.HTTP_200_OK)
                    else:
                        errors = dict()
                        errors["error"] = "The file is bigger than 2 MB"
                        return Response(errors, status=status.HTTP_400_BAD_REQUEST)

                # ...
                # do some stuff with uploaded file
                # ...
                errors = dict()
                errors.update(_image.errors)
                return Response(errors, status=status.HTTP_400_BAD_REQUEST)
            except ValidationError as e:
                return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
            except ValueError as e:
                return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
            except Exception as e:
                return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)

    def create(self, request):
            try:
                list_formats = ("image/png", "image/jpg", "image/jpeg", "image/gif",)
                try:
                    print("image")

                    file_obj = request.data['image']
                    print(type(file_obj))
                    print("after file_obj")
                    #f = magic.Magic(mime=True, uncompress=True)
                    with magic.Magic(flags=magic.MAGIC_MIME_TYPE) as m:
                        img_buffer = m.id_buffer(file_obj.read())
                        if img_buffer in list_formats:
                            pass
                        else:
                            errors = {"error": "Invalid Type", "type": img_buffer }
                            return Response(errors, status=status.HTTP_406_NOT_ACCEPTABLE)
                    print(str(file_obj.size))
                    if file_obj.size < 2500000:
                       url = True
                    else:
                        errors = {"error": "Image must be less than 2.5MB"}
                        return Response(errors, status=status.HTTP_406_NOT_ACCEPTABLE)
                except ValueError:
                    errors = {"error": str(ValueError)}
                    return Response(errors, status=status.HTTP_406_NOT_ACCEPTABLE)
                except Exception as e:
                    errors = {"error": str(e)}
                    return Response(errors, status=status.HTTP_406_NOT_ACCEPTABLE)
                _image = ImageSerializer(data=request.data)

                if _image.is_valid():
                    try:
                        url = upload_to_s3(file_obj)
                        #Profile.objects.filter(user=request.user).update(profile_image=url)
                    except Error as e:
                        error = dict()
                        error['error'] = str(e)
                        return Response(error, status=status.HTTP_409_CONFLICT)
                    except ValueError:
                        errors = {"error": str(ValueError)}
                        return Response(errors, status=status.HTTP_406_NOT_ACCEPTABLE)
                    except Exception as e:
                        errors = {"error": str(e)}
                        return Response(errors, status=status.HTTP_406_NOT_ACCEPTABLE)
                    if url:
                        message = {"url":url}
                        return Response(message,  status=status.HTTP_200_OK)
                    else:
                        errors = dict()
                        errors["error"] = "The file is bigger than 2 MB"
                        return Response(errors, status=status.HTTP_400_BAD_REQUEST)

                # ...
                # do some stuff with uploaded file
                # ...
                errors = dict()
                errors.update(_image.errors)
                return Response(errors, status=status.HTTP_400_BAD_REQUEST)
            except ValidationError as e:
                return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
            except ValueError as e:
                return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
            except Exception as e:
                return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)



class PublicProfilePageViewSet(viewsets.ModelViewSet):
    def get_object(self, pk):
        try:
            obj = User.objects.get(pk=pk)
            #self.check_object_permissions(self.request, obj)
            return obj
        except User.DoesNotExist:
            raise Http404
        except ValidationError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)
    authentication_classes = (OAuth2Authentication,)
    #permission_classes = (permissions.IsAuthenticated, UserInSessionPermission, UserTypeCompany,)

    def get_permissions(self):
        if self.request.method == 'GET':
            return (permissions.AllowAny(),)
        return (permissions.IsAuthenticated(), UserInSessionPermission(), UserTypeCompany(),)

    def retrieve(self, request, pk=None):
        try:
            tg_p = TgPermissions()
            tg_p.isSubscriptionValid(user)
            # user = User.objects.filter(pk=request.user.pk).select_related()
            page_serializer = GetProfileSerializer(self.get_object(pk=pk))
            mess = {"mes":"hola"}
            return Response(page_serializer.data, status=status.HTTP_200_OK)
        except ValidationError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)



class ProfilePageViewSet(viewsets.ModelViewSet):
    def get_object(self, pk):
        try:
            obj = User.objects.get(pk=pk)
            #self.check_object_permissions(self.request, obj)
            return obj
        except User.DoesNotExist:
            raise Http404
        except ValidationError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)
    authentication_classes = (OAuth2Authentication,)
    #permission_classes = (permissions.IsAuthenticated, UserInSessionPermission, UserTypeCompany,)

    def get_permissions(self):
        return (permissions.IsAuthenticated(), UserInSessionPermission(), UserTypeCompany(),)

    def retrieve(self, request, pk=None):
        try:
            if request.user and request.auth:
                print("hola")
                # page = ProfilePage.objects.get(user=request.user)
            else:
                return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)
            # user = User.objects.filter(pk=request.user.pk).select_related()
            tg_p = TgPermissions()
            tg_p.isSubscriptionValid(request.user)
            page_serializer = GetProfileSerializer(self.get_object(pk=request.user.pk))
            mess = {"mes":"hola"}
            return Response(page_serializer.data, status=status.HTTP_200_OK)
        except ValidationError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)

    def create(self, request):
        try:
            _page = ProfilePageSerializer(data=request.data)
            if len(request.data['profile_images']) > 3:
                errors = dict()
                errors["error"] = "You have permission to load 3 images"
                return Response(errors, status=status.HTTP_400_BAD_REQUEST)
            #requested_tags = RequestedTagSerializer(data=request.data)
            if _page.is_valid():
                _page.save(user=self.get_object(request.user.pk))
                return Response(_page.data, status=status.HTTP_201_CREATED)
            # Combine errors from both serializers.

            return Response(_page.errors, status=status.HTTP_400_BAD_REQUEST)
        except ValidationError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)

    def update(self, request,format=None):
        try:
            tg_p = TgPermissions()
            tg_p.isSubscriptionValid(request.user)
            page = ProfilePage.objects.filter(user_id=request.user.pk)
            _page = ProfilePageSerializer(page, data=request.data)
            if len(request.data['profile_images']) > 3:
                errors = dict()
                errors["error"] = "You have permission to load 3 images"
                return Response(errors, status=status.HTTP_400_BAD_REQUEST)
            #requested_tags = RequestedTagSerializer(data=request.data)
            if _page.is_valid():
                _page.save(user=self.get_object(request.user.pk))
                return Response(_page.data, status=status.HTTP_204_NO_CONTENT)
            # Combine errors from both serializers.

            return Response(_page.errors, status=status.HTTP_400_BAD_REQUEST)
        except ValidationError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)


class CategoryViewSet(viewsets.ViewSet):

    def get_permissions(self):
        if self.request.method == 'GET':
            return (permissions.AllowAny(),)

    def retrieve(self, request, pk=None):
        try:
            # user = User.objects.filter(pk=request.user.pk).select_related()
            #print(notifications)
            categories = Category.objects.all().order_by('category')
            serializer = CategorySerializer(categories, many=True)
            data = {"categories": serializer.data}
            return Response(data, status=status.HTTP_200_OK)
        except ValidationError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_409_CONFLICT)