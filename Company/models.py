#from django.db import models
from django.contrib.auth.models import User
from django.contrib.gis.db import models

# Create your models here.



class Location(models.Model):
    user = models.OneToOneField(User, primary_key=True, related_name='user_location')
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    country = models.CharField(max_length=100)
    address = models.CharField(max_length=200, blank=True)
    point = models.PointField(help_text="Represented as (longitude, latitude)", srid=4326)
        # You MUST use GeoManager to make Geo Queries
    objects = models.GeoManager()


class TagOffered(models.Model):
    name = models.CharField(max_length=50)
    tagoffered = models.ManyToManyField(User, related_name='user_tagoffered', through='Company_Tags_Offered',)
    requiredtag = models.ManyToManyField(User, related_name='user_requiretag', through='Company_Tags_Requiere',)
    #user = models.ManyToManyField(User)

class Company_Tags_Requiere(models.Model):
    profile_id = models.ForeignKey(User)
    tag_id = models.ForeignKey(TagOffered)
    date_joined = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True)



class Company_Tags_Offered(models.Model):
    profile_id = models.ForeignKey(User)
    tag_id = models.ForeignKey(TagOffered)
    date_joined = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True)




#class TagOfferedMap(models.Model):
 #   user = models.ForeignKey(User)
  #  tag_Offered = models.ForeignKey(TagOffered)


# class RequestedTag(models.Model):
#     name = models.CharField(max_length=50)
    #user = models.ManyToManyField(User)

#class RequestedTagMap(models.Model):
    #user = models.ForeignKey(User)
    #requested_tag = models.ForeignKey(RequestedTag)

#class Category(models.Model):
    #category

class ProfilePage(models.Model):
    user = models.OneToOneField(User, primary_key=True, related_name='profile_page')
    description = models.CharField(max_length=500)
    web = models.CharField(max_length=250)

class PageImages(models.Model):
    p_page = models.ForeignKey(ProfilePage, related_name='profile_images')
    url = models.CharField(max_length=250)
    image_description = models.CharField(max_length=250)

# class CheckIn(models.Model):
#     company = models.ForeignKey(User, related_name='checkin')
#     client = models.ForeignKey(User)
#     checkin_date = models.DateField(auto_now=True)
#
# class Rate(models.Model):
#     company = models.ForeignKey(User, related_name='rate')
#     date= models.DateField(auto_now=True)
#     rate = models.IntegerField()
#     user = models.ForeignKey(User)
class Notification(models.Model):
    user = models.ForeignKey(User, related_name="user_notification")
    NOTIFICATION_TYPES = ((0, 'MESSAGE'),(1, 'MATCH'), (2, 'CHECK-IN'),)
    notification_type = models.IntegerField(null=True, choices=NOTIFICATION_TYPES)
    description = models.CharField(max_length=500, null=True)
    createdAt = models.DateTimeField(auto_now_add=True)
    match= models.IntegerField(null=True)

class Category(models.Model):
    category = models.CharField(max_length=200, null=False)
    description = models.CharField(max_length=500, null=True)