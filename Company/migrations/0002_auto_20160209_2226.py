# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('Company', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='location',
            name='user',
            field=models.OneToOneField(related_name='user_location', serialize=False, primary_key=True, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='profilepage',
            name='user',
            field=models.OneToOneField(related_name='profile_page', serialize=False, primary_key=True, to=settings.AUTH_USER_MODEL),
        ),
    ]
