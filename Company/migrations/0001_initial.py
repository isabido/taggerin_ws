# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('category', models.CharField(max_length=200)),
                ('description', models.CharField(null=True, max_length=500)),
            ],
        ),
        migrations.CreateModel(
            name='Company_Tags_Offered',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('date_joined', models.DateTimeField(auto_now=True)),
                ('active', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='Company_Tags_Requiere',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('date_joined', models.DateTimeField(auto_now=True)),
                ('active', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('user', models.OneToOneField(primary_key=True, to=settings.AUTH_USER_MODEL, serialize=False)),
                ('city', models.CharField(max_length=100)),
                ('state', models.CharField(max_length=100)),
                ('country', models.CharField(max_length=100)),
                ('address', models.CharField(blank=True, max_length=200)),
                ('point', django.contrib.gis.db.models.fields.PointField(help_text='Represented as (longitude, latitude)', srid=4326)),
            ],
        ),
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('notification_type', models.IntegerField(choices=[(0, 'MESSAGE'), (1, 'MATCH'), (2, 'CHECK-IN')], null=True)),
                ('description', models.CharField(null=True, max_length=500)),
                ('createdAt', models.DateTimeField(auto_now_add=True)),
                ('match', models.IntegerField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='PageImages',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('url', models.CharField(max_length=250)),
                ('image_description', models.CharField(max_length=250)),
            ],
        ),
        migrations.CreateModel(
            name='ProfilePage',
            fields=[
                ('user', models.OneToOneField(primary_key=True, to=settings.AUTH_USER_MODEL, serialize=False)),
                ('description', models.CharField(max_length=500)),
                ('web', models.CharField(max_length=250)),
            ],
        ),
        migrations.CreateModel(
            name='TagOffered',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('requiredtag', models.ManyToManyField(to=settings.AUTH_USER_MODEL, related_name='user_requiretag', through='Company.Company_Tags_Requiere')),
                ('tagoffered', models.ManyToManyField(to=settings.AUTH_USER_MODEL, related_name='user_tagoffered', through='Company.Company_Tags_Offered')),
            ],
        ),
        migrations.AddField(
            model_name='pageimages',
            name='p_page',
            field=models.ForeignKey(to='Company.ProfilePage', related_name='profile_images'),
        ),
        migrations.AddField(
            model_name='notification',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='user_notification'),
        ),
        migrations.AddField(
            model_name='company_tags_requiere',
            name='profile_id',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='company_tags_requiere',
            name='tag_id',
            field=models.ForeignKey(to='Company.TagOffered'),
        ),
        migrations.AddField(
            model_name='company_tags_offered',
            name='profile_id',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='company_tags_offered',
            name='tag_id',
            field=models.ForeignKey(to='Company.TagOffered'),
        ),
    ]
