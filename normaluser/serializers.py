__author__ = 'isabido'

from rest_framework import serializers
from django.contrib.auth.models import User
from users.models import Profile


class NormalUser(serializers.ModelSerializer):
    profile_image = serializers.URLField(source='profile.profile_image')
    gender = serializers.CharField(source='profile.gender')
    birthday = serializers.DateField(source='profile.birthday')
    #user_type = serializers.IntegerField(source='profile.user_type')
    class Meta:
        model = User
        fields = ['id','username','email', 'password', 'first_name','last_name','profile_image', 'gender', 'birthday',]

    def create(self, validated_data):
        _post = validated_data.pop('profile')
        user = User.objects.create_user(**validated_data)
        user.is_active = False
        user.save()
        Profile.objects.create(user=user, user_type=1,**_post)
        return user

    def update(self, instance, validated_data):
        _post = validated_data.pop('profile')
        instance.username = validated_data.get('username', instance.username)
        instance.email = validated_data.get('email', instance.email)
        instance.set_password(validated_data.get('password', instance.password))
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        #instance.password = validated_data.get('password', instance.password)
        instance.save()
        prof = Profile.objects.get(pk=instance.id)
        prof.profile_image = _post['profile_image']
        prof.birthday = _post['birthday']

        if prof.user_type != 1:
            prof.user_type = 1
        prof.save()


        #Profile.objects.update(user=user, profile_image = _post['profile_image'])

        return instance


class NormalProfileUser(serializers.ModelSerializer):
   class Meta:
        model = Profile
        #profile_image = Field(source='profile.profile_image')
        fields = ['profile_image',]

        def create(self, validated_data):
            _post = validated_data.pop('profile_image')
            user = User.objects.create_user(**validated_data)
            return Profile(user=user)
