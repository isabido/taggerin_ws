from django.http import Http404
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response
from users.permissions import UserInSessionPermission
from rest_framework import permissions


from normaluser.serializers import NormalUser
from oauth2_provider.ext.rest_framework import OAuth2Authentication

# Create your views here.
#class NormalSignUp(APIView):
class NormalUserViewSet(viewsets.ViewSet):
    """
    A viewset for viewing and editing user instances.
    """
    def get_object(self, pk):
        try:
            obj = User.objects.get(pk=pk)
            #self.check_object_permissions(self.request, obj)
            return obj
        except User.DoesNotExist:
            raise Http404

    #authentication_classes = (OAuth2Authentication,)
    #permission_classes = (permissions.IsAuthenticated, UserInSessionPermission,)

    def create(self, request):
       # permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
        # Both UserProfileSerializer and UserProfileSerializer are required
        # in order to validate and save data on their associated models.
        user_serializer = NormalUser(data=request.data)
        #user_profile_serializer = NormalProfileUser(data=request.data)

        if user_serializer.is_valid():
            #user = user_serializer.save()
            user_serializer.save()

            return Response(user_serializer.data, status=status.HTTP_201_CREATED)
        # Combine errors from both serializers.
        errors = dict()
        errors.update(user_serializer.errors)
        return Response(errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None):
        if request.user and request.auth:
            user = self.get_object(request.user.pk)
        else:
            return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)
        user_serializer = NormalUser(user, data=request.data)
        if user_serializer.is_valid():
            user_serializer.save()
            #user_serializer.save(#owner=self.request.user)
            return Response(user_serializer.data, status=status.HTTP_200_OK)
        errors = dict()
        errors.update(user_serializer.errors)
        return Response(errors, status=status.HTTP_400_BAD_REQUEST)